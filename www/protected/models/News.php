<?php

/**
 * This is the model class for table "news".
 *
 * @property string $title
 * @property string $brief
 * @property string $body
 * @property integer $published
 */
class News extends BaseModel
{
    /**
     * @var array
     */
    protected $attachments = array();

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('is_visible', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('brief', 'length', 'max' => 1000),
            array('body', 'length', 'max' => 2000),
            array('published', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, brief, body, published, is_visible', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'attachments' => array(self::HAS_MANY, 'NewsAttachment', 'news_id'),
        );
    }

    public function setAttributes($values, $safeOnly = true)
    {
        parent::setAttributes($values, $safeOnly);

        $this->attachments = array();
        foreach ($values['_relations']['NewsAttachment'] as $data) {
            if (!empty($data['entity_id'])) {
                $this->attachments[] = $data;
            }
        }
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->scenario !== BaseModel::ACTION_VISIBILITY) {
                $this->published = strtotime($this->published);
            }
            return true;
        }
        return false;
    }

    public function afterSave()
    {
        parent::afterSave();

        if ($this->scenario !== BaseModel::ACTION_VISIBILITY) {
            NewsAttachment::model()->deleteAll(array(
                'condition' => 'news_id = :id',
                'params' => array(':id' => $this->id),
            ));
            foreach ($this->attachments as $data) {
                $model = new NewsAttachment();
                $model->setAttributes($data);
                $model->news_id = $this->id;
                $model->save(false);
            }
        }
    }

    public function behaviors()
    {
        return array(
            'defaultBehavior' => array(
                'class' => 'DefaultModelBehavior',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'title' => 'Заголовок',
            'brief' => 'Краткое описание',
            'body' => 'Текст',
            'published' => 'Дата публикации',
            'attachments' => 'Изображения',
            'is_visible' => 'Активность',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('brief', $this->brief, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('published', $this->published, true);
        $criteria->compare('is_visible', $this->is_visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return News the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getPublicationDate()
    {
        return date('d.m.Y', $this->published);
    }

    public function getViewUrl()
    {
        return Yii::app()->controller->createUrl('/news/view', array('id' => $this->id));
    }
}
