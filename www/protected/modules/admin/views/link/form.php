<?
/**
 * @var $this AdminController
 * @var $form YActiveForm
 * @var $model Club
 */
?>
<div class="col-sm-5">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'role' => 'form',
        )
    ));
    ?>

    <? echo CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'title', array('for' => 'title')); ?>
        <?=$form->textField($model, 'title', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'link', array('for' => 'link')); ?>
        <?=$form->textField($model, 'link', array('class' => 'form-control', 'placeholder' => 'http://')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'image', array('for' => '')); ?>
        <?
        $this->widget('admin.widgets.FileUploaderWidget',
            array(
                'model' => $model,
                'attribute' => 'image',
                'htmlOptions' => array(
                    'data-show-upload' => 'false',
                    'multiple' => false,
                    'class' => 'file'
                ),
            )
        );
        ?>
    </div>

    <? if(!empty($model->id)): ?>
        <div class="form-group">
            <? echo $model->getImageHtml(); ?>
        </div>
    <? endif; ?>

    <?=YHtml::saveButton(); ?>
    <?=YHtml::cancelButton(); ?>

    <? $this->endWidget(); ?>
</div>