<?
/**
 * @var array $data
 */
?>
<? foreach($data as $item): ?>
    <? /* @var array $item */ ?>
    <div class="news-block">
        <?=$item['content']?>
        <div class="news-block-footer">
            <?=ViewHelper::getDate($item['published'])?> | <a href="<?=$item['viewUrl']?>">Подробнее</a>
        </div>
    </div>
<? endforeach; ?>