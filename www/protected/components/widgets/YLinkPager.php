<?php

Yii::import('system.web.widgets.pagers.CLinkPager');

/**
 * Class YLinkPager
 */
class YLinkPager extends CLinkPager
{
    public $header = '';

    public $prevPageLabel = '&laquo; назад';
    public $nextPageLabel = 'вперед &raquo;';

    public $lastPageLabel = 'последняя';
    public $firstPageLabel = 'первая';

    public $selectedPageCssClass = 'active';
    public $hiddenPageCssClass = 'disabled';

    public $cssFile = false;

    public $htmlOptions = array(
        'class' => 'pagination'
    );
}