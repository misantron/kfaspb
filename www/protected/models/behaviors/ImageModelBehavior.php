<?php

class ImageModelBehavior extends CActiveRecordBehavior
{
    const
        SIZE_PREFIX_BASE  = '',
        SIZE_PREFIX_TINY  = '_t',
        SIZE_PREFIX_SMALL = '_s',
        SIZE_PREFIX_MEDIUM = '_m'
    ;

    protected $_basePath = 'webroot.';
    protected $_thumbDirectory = 'thumb';
    protected $_defaultImage = 'no_image.png';

    protected $_scenarioList = array(
        BaseModel::ACTION_INSERT,
        BaseModel::ACTION_UPDATE
    );

    public $baseDirectory  = 'data.image';
    public $field = 'image';
    public $thumbSizes = array(
        self::SIZE_PREFIX_TINY  => 50,
        self::SIZE_PREFIX_SMALL => 80,
        self::SIZE_PREFIX_MEDIUM => 120,
    );

    protected function saveUploadedImage()
    {
        if($file = CUploadedFile::getInstance($this->owner, $this->field)){
            $field = $this->getField();
            if(!$this->owner->isNewRecord && !empty($field)){
                $this->deleteUploadedImage();
            }
            $path = $this->getImagePath(true);
            $name = UtilHelper::generateFileName() . '.' . $file->getExtensionName();
            if($file->saveAs($path . $name))
            {
                if(!empty($this->thumbSizes) && is_array($this->thumbSizes)){
                    Yii::app()->imageHandler
                        ->load($path . $name)
                        ->adaptiveThumb($this->thumbSizes[self::SIZE_PREFIX_TINY], $this->thumbSizes[self::SIZE_PREFIX_TINY])
                        ->save($this->getThumbPath(self::SIZE_PREFIX_TINY, true) . $name)
                        ->reload()
                        ->adaptiveThumb($this->thumbSizes[self::SIZE_PREFIX_SMALL], $this->thumbSizes[self::SIZE_PREFIX_SMALL])
                        ->save($this->getThumbPath(self::SIZE_PREFIX_SMALL, true) . $name)
                        ->reload()
                        ->adaptiveThumb($this->thumbSizes[self::SIZE_PREFIX_MEDIUM], $this->thumbSizes[self::SIZE_PREFIX_MEDIUM])
                        ->save($this->getThumbPath(self::SIZE_PREFIX_MEDIUM, true) . $name);
                }

                $this->owner->{$this->field} = $name;
            }
        }
    }

    protected function deleteUploadedImage()
    {
        $field = $this->getField();
        if(file_exists($this->getImagePath(true) . $field)){
            unlink($this->getImagePath(true) . $field);
        }
        if(file_exists($this->getThumbPath(self::SIZE_PREFIX_TINY, true) . $field)){
            unlink($this->getThumbPath(self::SIZE_PREFIX_TINY, true) . $field);
        }
        if(file_exists($this->getThumbPath(self::SIZE_PREFIX_SMALL, true) . $field)){
            unlink($this->getThumbPath(self::SIZE_PREFIX_TINY, true) . $field);
        }
        if(file_exists($this->getThumbPath(self::SIZE_PREFIX_MEDIUM, true) . $field)){
            unlink($this->getThumbPath(self::SIZE_PREFIX_MEDIUM, true) . $field);
        }
    }

    /**
     * @return string|null
     */
    protected function getField()
    {
        $field = $this->field;
        return $this->owner->{$field};
    }

    /**
     * @param bool $absolute
     * @return string
     */
    protected function getImagePath($absolute = false)
    {
        return $absolute ?
            Yii::getPathOfAlias($this->_basePath . $this->baseDirectory) . '/' :
            '/' . str_replace('.', '/', $this->baseDirectory) . '/';
    }

    /**
     * @param string $size
     * @param bool $absolute
     * @return string
     */
    protected function getThumbPath($size = self::SIZE_PREFIX_TINY, $absolute = false)
    {
        return $absolute ?
            Yii::getPathOfAlias($this->_basePath . $this->baseDirectory . '.' . $this->_thumbDirectory . $size) . '/' :
            '/' . str_replace('.', '/', $this->baseDirectory) . '/' . $this->_thumbDirectory . $size . '/';
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        $field = $this->getField();
        $exists = false;
        if(!empty($field)){
            $exists = file_exists($this->getImagePath(true) . $field);
        }
        return $this->getImagePath(false) . ($exists ? $field : $this->_defaultImage);
    }

    /**
     * @param string $size
     * @return string
     */
    public function getThumbUrl($size = self::SIZE_PREFIX_TINY)
    {
        $field = $this->getField();
        $exists = false;
        if(!empty($field)){
            $exists = file_exists($this->getThumbPath($size, true) . $field);
        }
        return $this->getThumbPath($size, false) . ($exists ? $field : $this->_defaultImage);
    }

    /**
     * @param int $height
     * @param int $width
     * @return string
     */
    public function getImageHtml($height = 0, $width = 0)
    {
        $field = $this->getField();
        $htmlOptions = array();
        if($width > 0) $htmlOptions['width'] = (int)$width;
        if($height > 0) $htmlOptions['height'] = (int)$height;
        return CHtml::image($this->getImageUrl(), ($field ? $field : $this->_defaultImage), $htmlOptions);
    }

    /**
     * @param string $size
     * @param int $height
     * @param int $width
     * @return string
     */
    public function getThumbHtml($size = self::SIZE_PREFIX_TINY, $height = 0, $width = 0)
    {
        $field = $this->getField();
        $htmlOptions = array('class' => '');
        if($width > 0) $htmlOptions['width'] = (int)$width;
        if($height > 0) $htmlOptions['height'] = (int)$height;
        return CHtml::image($this->getThumbUrl($size), ($field ? $field : $this->_defaultImage), $htmlOptions);
    }

    public function beforeSave($event)
    {
        $this->saveUploadedImage();
    }

    public function afterDelete($event)
    {
        $this->deleteUploadedImage();
    }

    /**
     * @param BaseModel $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);

        if(in_array($owner->scenario, $this->_scenarioList)){
            $fileValidator = CValidator::createValidator('file', $owner, $this->field, $this->getValidationRules());
            $owner->getValidatorList()->add($fileValidator);
        }
    }

    /**
     * @return array
     */
    public function getValidationRules()
    {
        return array(
            'types' => 'jpg,png,gif',
            'wrongType' => 'Файл "{file}" имеет недопустимый тип. Для загрузки возможны только файлы с расширениями: {extensions}.',
            'maxSize' => 2097152,
            'tooLarge' => 'Файл "{file}" слишком большой. Максимальный размер загружаемого файла - {limit} байт.',
            'allowEmpty' => true,
            'maxFiles' => 1,
            'tooMany' => 'Можно загрузить только {limit} файл.',
        );
    }
}