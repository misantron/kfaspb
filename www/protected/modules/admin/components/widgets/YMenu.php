<?php

class YMenu extends CMenu
{
    public $encodeLabel = false;

    public $htmlOptions = array(
        'class' => 'nav nav-sidebar',
    );
}