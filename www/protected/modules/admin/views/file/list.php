<?
    /**
     * @var FileController $this
     * @var CActiveDataProvider $dataProvider
     */
?>
    <div class="row text-right">
        <?=YHtml::createButton($this->createUrl('create'))?>
    </div>
<?
    $this->widget('YGridView', array(
        'dataProvider' => $dataProvider,
        'buttons' => $this->getAction()->buttons,
        'columns' => array(
            array(
                'name' => 'Название',
                'value' => '$data->name',
            ),
            array(
                'name' => 'Раздел',
                'value' => 'DictHelper::getById(\'document_group\', $data->group_id)',
            ),
            array(
                'name' => 'Описание',
                'value' => '$data->description',
            ),
            array(
                'name' => 'Тип',
                'type' => 'raw',
                'value' => 'FileHelper::getType($data->extension)',
            ),
            array(
                'name' => 'Размер',
                'type' => 'raw',
                'value' => 'FileHelper::getSize($data->byte_size)',
            ),
        ),
    ));
?>