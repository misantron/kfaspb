<?php

class SystemController extends AdminController
{
    public function actionError()
    {
        if($error = Yii::app()->errorHandler->error) {
            if(Yii::app()->request->isAjaxRequest){
                $response = new AjaxResponse(false);
                $response
                    ->addError($error['message'])
                    ->render();
            }
            $this->setPageTitle('Ошибка ' . $error['code'] . ' - ' . HttpStatusCode::getMessage($error['code']));
            $this->render('error', $error);
        }
    }
}