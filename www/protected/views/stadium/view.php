<?
/**
 * @var Stadium $model
 */
?>
<div class="content-block">
    <h1>Стадион - <?=CHtml::encode($model->name)?></h1>

    <?
    $this->widget('application.widgets.GoogleMapWidget', array(
        'markers' => array(
            array(
                'title' => 'Стадион "' . $model->name . '"<br />' . $model->address,
                'position' => array(
                    'lat' => $model->lat,
                    'lon' => $model->lon,
                ),
            ),
        ),
        'zoom' => 16,
    ));
    ?>

    <div>
        <strong>Адрес:</strong> <?=$model->address?><br />
    </div>
    <h3>Поля</h3>
    Тип: <?php echo DictHelper::getById('field_type', $model->field_type); ?>
    Размеры: <?php echo $model->field_size; ?>
    <h3>Клубы</h3>
    <?php foreach($model->clubs as $club): ?>
        <?php /** @var Club $club */ ?>
        <img src="<?php echo $club->getThumbUrl(); ?>" alt="" />
        <?php echo $club->name; ?> (
        <?php foreach($club->teams as $teamId): ?>
            <?php echo DictHelper::getById('teams', $teamId); ?>,
        <?php endforeach; ?>
        ) команды
    <?php endforeach; ?>
</div>