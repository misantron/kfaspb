<?
/**
 * @var $this AdminController
 * @var $form CActiveForm
 * @var $model File
 */
?>
<div class="col-sm-5">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
        'htmlOptions' => array(
            'role' => 'form',
            'enctype' => 'multipart/form-data',
        )
    ));
    ?>

    <?=CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'name', array('for' => '')); ?>
        <?=$form->textField($model, 'name', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'group_id', array('for' => '')); ?>
        <?=$form->dropDownList($model, 'group_id', DictHelper::getList('document_group'), array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'description', array('for' => '')); ?>
        <?=$form->textArea($model, 'description', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'file', array('for' => 'file')); ?>
        <?
        $this->widget('admin.widgets.FileUploaderWidget',
            array(
                'model' => $model,
                'attribute' => 'file',
                'htmlOptions' => array(
                    'data-show-upload' => 'false',
                    'multiple' => false,
                    'class' => 'file'
                ),
            )
        );
        ?>
    </div>

    <?=YHtml::saveButton(); ?>
    <?=YHtml::cancelButton(); ?>

    <? $this->endWidget(); ?>
</div>