<?php

class DatePickerWidget extends CInputWidget
{
    public $options = array();

    public function init()
    {
        list($this->name, $this->id) = $this->resolveNameID();

        $this->options = array(
            'language' => 'ru',
            'format' => 'dd.mm.yyyy',
            'autoclose' => 'true',
            'weekStart' => 1,
            'startView' => 2,
        );

        $timestamp = YHtml::resolveValue($this->model, $this->attribute);

        $this->htmlOptions['value'] = date('d.m.Y', $timestamp ?: time());

        echo YHtml::openTag('div', array(
            'id' => $this->id,
            'class' => 'input-group date col-sm-4',
            'data-date' => date('d.m.Y'),
        ));
        echo YHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
        echo YHtml::tag('span', array('class' => 'input-group-addon add-on'), '<i class="glyphicon glyphicon-calendar"></i>');
        echo YHtml::closeTag('div');

        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        $js = '$("#' . $this->id . '").datepicker(' . CJavaScript::encode($this->options) . ')';

        Yii::app()->clientScript
            ->registerPackage('date-picker')
            ->registerScript(__CLASS__ . '#' . $this->id, $js, CClientScript::POS_READY);
    }

}