<?php

class NewsController extends AdminController
{
    protected $_modelClass = 'News';
    protected $_childModelClasses = array('NewsAttachment');
    public $title = 'Новости';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
                'dataProviderConfig' => array(
                    'criteria' => array(
                        'order' => 'published DESC'
                    ),
                ),
            ),
            'create' => array(
                'class' => 'CreateAction',
            ),
            'edit' => array(
                'class' => 'EditAction',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
            'visibility' => array(
                'class' => 'VisibilityAction',
            ),
        );
    }
}