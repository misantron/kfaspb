<?php

class TextEditorWidget extends CInputWidget
{
    public $options = array();

    public function init()
    {
        list($this->name, $this->id) = $this->resolveNameID();

        $this->options = array(
            'lang' => 'ru-RU',
            'height' => 200,
            'toolbar' => array(
                array('style', array('bold', 'italic', 'underline', 'strikethrough', 'clear')),
                array('color', array('color')),
                array('format', array('ul', 'ol', 'paragraph')),
                array('code', array('codeview')),
                array('help', array('help')),
            ),
        );

        echo YHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);

        $this->registerClientScripts();
    }

    protected function registerClientScripts()
    {
        $js = '$("#' . $this->id . '").summernote(' . CJavaScript::encode($this->options) . ')';

        Yii::app()->clientScript
            ->registerPackage('summer-note')
            ->registerScript(__CLASS__ . '#' . $this->id, $js, CClientScript::POS_READY);
    }
}