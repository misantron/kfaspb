<?
/**
 * @var array $imageList
 */
?>
<div class="form-inline">
    <? foreach($this->items as $key => $model): ?>
        <? /* @var NewsAttachment $model */ ?>
        <div id="attachment-container-<?=$key?>" style="margin-bottom: 5px">
            <div class="input-group">
                <div class="input-group-addon"><?=$model->getAttributeLabel('type')?></div>
                <?=YHtml::activeDropDownList($model, '['.$key.']type', DictHelper::getList('news_attachment'), array(
                    'class' => 'form-control type-selector',
                    'data-key' => $key,
                    'options' => array(
                        $model->type => array('selected' => true)
                    ),
                ))?>
            </div>
            <div class="input-group">
                <?=YHtml::htmlButton('Выбрать изображение', array(
                    'class' => 'btn btn-default hidden img-selector-' . $key,
                    'data-toggle' => 'modal',
                    'data-target' => '.image-select-' . $key,
                ))?>
                <?=YHtml::htmlButton('Выбрать арбитра', array(
                    'class' => 'btn btn-default hidden referee-selector-' . $key,
                    'data-toggle' => 'modal',
                    'data-target' => '.referee-select-' . $key,
                ))?>
            </div>
            <div class="input-group">
                <div class="input-group-addon"><?=$model->getAttributeLabel('link')?></div>
                <?=YHtml::activeTextField($model, '['.$key.']link', array(
                    'class' => 'form-control',
                    'value' => $model->link,
                ))?>
            </div>
            <?=YHtml::activeHiddenField($model, '['.$key.']entity_id', array(
                'value' => $model->entity_id
            ))?>
        </div>
        <div class="modal fade image-select-<?=$key?>" tabindex="-1" role="dialog" aria-labelledby="imageSelectLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span>
                        </button>
                        <h4 class="modal-title" id="imageSelectLabel">Выберите изображение</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <? foreach($imageList as $imageModel): ?>
                                <? /* @var Image $model */ ?>
                                <div class="col-xs-6 col-md-3">
                                    <div class="thumbnail">
                                        <img src="<?=$imageModel->getThumbUrl()?>" data-key="<?=$key?>" data-id="<?=$imageModel->id?>" alt="<?=$imageModel->name?>">
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade referee-select-<?=$key?>" tabindex="-1" role="dialog" aria-labelledby="refereeSelectLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span>
                        </button>
                        <h4 class="modal-title" id="refereeSelectLabel">Выберите профиль</h4>
                    </div>
                    <div class="modal-body">
                        <?=YHtml::label('Поиск', 'name', array())?>
                        <?=YHtml::textField('referee-name', '', array(
                            'class' => 'typeahead referee-search form-control',
                            'autocomplete' => 'off',
                            'data-provide' => 'typeahead',
                            'data-key' => $key
                        ))?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>