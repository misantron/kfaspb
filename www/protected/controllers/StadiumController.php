<?php

class StadiumController extends BaseController
{
    public $defaultAction = 'list';
    protected $_modelClass = 'Stadium';

    public function actionList()
    {
        $modelClass = $this->_modelClass;
        $models = $modelClass::model()->visible()->findAll(array(
            'order' => 'name'
        ));

        $markers = array();
        foreach ($models as $model) {
            /* @var Stadium $model */
            if(empty($model->lat) || empty($model->lon)){
                continue;
            }
            $markers[] = array(
                'title' => 'Стадион "' . $model->name . '"<br />' . $model->address,
                'position' => array(
                    'lat' => $model->lat,
                    'lon' => $model->lon,
                )
            );
        }

        $this->render('list', array(
            'markers' => $markers,
            'models' => $models,
        ));
    }

    public function actionView()
    {
        $modelClass = $this->_modelClass;
        $id = (int)Yii::app()->request->getParam('id', 0);
        $model = $modelClass::model()->visible()->findByPk($id);
        if($model === null){
            $this->returnUrlRedirect();
        }

        $this->render('view', array(
            'model' => $model
        ));
    }
}