<?php

class DeleteAction extends BaseAction
{
    public function run()
    {
        /**
         * @var AdminController $controller
         * @var BaseModel $model
         */
        $controller = $this->getController();
        $modelClass = $controller->getModelClass();

        $model = $modelClass::model()->getById();
        if($model === null){
            $controller->setErrorMessage(Yii::t('', 'Объект не найден'));
            $controller->returnUrlRedirect();
        }
        $model->setScenario(BaseModel::ACTION_DELETE);

        if($model->delete()){
            $controller->setSuccessMessage(Yii::t('', 'Объект успешно удален'));
        } else {
            $controller->setErrorMessage(Yii::t('', 'Ошибка при удалении объекта'));
        }
        $controller->returnUrlRedirect();
    }
}