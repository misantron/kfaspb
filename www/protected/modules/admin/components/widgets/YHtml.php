<?php

/**
 * Class Html
 */
class YHtml extends CHtml
{
    /**
     * @return string
     */
    public static function saveButton()
    {
        return self::submitButton('Сохранить', array('class' => 'btn btn-info'));
    }

    /**
     * @return string
     */
    public static function cancelButton()
    {
        return self::button('Назад', array(
            'class' => 'btn btn-default',
            'onclick' => 'history.go(-1)',
        ));
    }

    /**
     * @param string $url
     * @return string
     */
    public static function createButton($url)
    {
        return self::link('<i class="glyphicon glyphicon-plus"></i>&nbsp;Добавить', $url, array('class' => 'btn btn-primary'));
    }
} 