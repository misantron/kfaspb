<?php

/**
 * Class BaseModel
 *
 * @property integer $id
 * @property integer $is_visible
 * @property integer $modified
 */
class BaseModel extends CActiveRecord
{
    const
        ACTION_INSERT = 'insert',
        ACTION_UPDATE = 'update',
        ACTION_VISIBILITY = 'visibility',
        ACTION_DELETE = 'delete'
    ;

    const
        STATE_NOT_VISIBLE = 0,
        STATE_VISIBLE = 1
    ;

    protected $_primaryKey = 'id';

    /**
     * @return BaseModel|null
     */
    public function getById()
    {
        $id = Yii::app()->request->getParam($this->_primaryKey, 0);
        return $this->findByPk($id);
    }

    public function scopes()
    {
        return array(
            'visible' => array(
                'condition' => 'is_visible = :state',
                'params' => array(':state' => self::STATE_VISIBLE)
            )
        );
    }

    /**
     * @param string $className
     * @return BaseModel
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return property_exists($this, 'name') ? $this->name : '';
    }
}