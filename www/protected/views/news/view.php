<?
/**
 * @var News $model
 */
?>
<div class="content-block">
    <? if(!empty($model->title)): ?>
    <h1><?=$model->title?></h1>
    <? endif; ?>
    <div>
        <? if(!empty($model->image_path)): ?>
            <img src="<?=$model->image_path?>" class="news-block-image">
        <? endif; ?>
        <?=nl2br($model->body)?>
    </div>
</div>