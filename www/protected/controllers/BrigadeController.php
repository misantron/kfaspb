<?php

class BrigadeController extends BaseController
{
    protected $_modelClass = 'Brigade';

    public function actionList()
    {
        $modelClass = $this->_modelClass;
        $models = $modelClass::model()->visible()->findAll();

        $models = array_chunk($models, 4);

        $this->render('list', array(
            'models' => $models,
        ));
    }

    public function actionView()
    {
        $modelClass = $this->_modelClass;
        $id = (int)Yii::app()->request->getParam('id', 0);

        $model = $modelClass::model()->visible()->findByPk($id);
        if($model === null){
            $this->returnUrlRedirect();
        }

        $this->setPageTitle('Состав бригады #' . $id);

        $this->render('view', array(
            'model' => $model,
        ));
    }
}