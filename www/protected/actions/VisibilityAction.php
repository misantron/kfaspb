<?php

class VisibilityAction extends BaseAction
{
    public function run()
    {
        /**
         * @var AdminController $controller
         * @var BaseModel $model
         */
        $controller = $this->getController();
        $modelClass = $controller->getModelClass();

        $model = $modelClass::model()->getById();
        if ($model === null) {
            $controller->setErrorMessage(Yii::t('', 'Объект не найден'));
            $controller->returnUrlRedirect();
        }
        $model->setScenario(BaseModel::ACTION_VISIBILITY);

        $model->is_visible = $model->is_visible == BaseModel::STATE_NOT_VISIBLE ?
            BaseModel::STATE_VISIBLE :
            BaseModel::STATE_NOT_VISIBLE;

        if ($model->save()) {
            $controller->setSuccessMessage(Yii::t('', 'Видимость объекта успешно изменена'));
        } else {
            $controller->setErrorMessage(Yii::t('', 'Ошибка при измнении видимости объекта'));
        }
        $controller->returnUrlRedirect();
    }
}