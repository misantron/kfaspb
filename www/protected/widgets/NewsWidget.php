<?php

class NewsWidget extends CWidget
{
    public $limit;
    public $items = array();

    public function init()
    {
        $this->limit = 10;

        $criteria = new CDbCriteria(array(
            'limit' => $this->limit,
            'order' => 'published DESC'
        ));

        $data = array();
        $models = News::model()->with('attachments')->visible()->findAll($criteria);
        foreach ($models as $model) {
            /** @var News $model */
            $attachmentCount = sizeof($model->attachments);
            $attachments = array();
            if ($attachmentCount > 0) {
                $entityUrl = $title = '';
                foreach ($model->attachments as $key => $item) {
                    if ($item->type == 1) {
                        /** @var Image $imageModel */
                        $imageModel = Image::model()->visible()->findByPk($item->entity_id);
                        $entityUrl = $imageModel->getThumbUrl();
                        $title = $imageModel->name;
                    } elseif ($item->type == 2) {
                        /** @var Referee $refereeModel */
                        $refereeModel = Referee::model()->visible()->findByPk($item->entity_id);
                        $entityUrl = $refereeModel->getThumbUrl(ImageModelBehavior::SIZE_PREFIX_MEDIUM);
                        $title = $refereeModel->getFullName();
                    }
                    $attachments[] = array(
                        'link' => $item->link,
                        'entityUrl' => $entityUrl,
                        'title' => $title,
                    );
                }
            }
            $data[] = array(
                'title' => $model->title,
                'published' => $model->published,
                'viewUrl' => $model->getViewUrl(),
                'content' => $this->render('news/column_' . $attachmentCount, array(
                    'title' => $model->title,
                    'brief' => empty($model->brief) ? mb_substr($model->body, 0, 200, ViewHelper::$encoding) : $model->brief,
                    'attachments' => $attachments
                ), true),
            );
        }

        $this->render('news', array(
            'data' => $data,
        ));
    }
}