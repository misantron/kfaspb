<?php

class DefaultModelBehavior extends CActiveRecordBehavior
{
    const DATE_TIME_FORMAT = 'd.m.Y H:i';

    public $modifiedField = 'modified';
    public $visibleField = 'is_visible';

    public function beforeSave($event)
    {
        $this->owner->{$this->modifiedField} = time();
    }

    public function afterFind($event)
    {

    }

    /**
     * @return string
     */
    public function getModifiedDateTime()
    {
        if(!$this->owner->{$this->modifiedField}){
            return '-';
        }
        return date(self::DATE_TIME_FORMAT, $this->owner->{$this->modifiedField});
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return (int)$this->owner->{$this->visibleField} === BaseModel::STATE_VISIBLE;
    }
}