<?
/**
 * @var $this AdminController
 * @var $form YActiveForm
 * @var $model Image
 */
?>
<div class="col-sm-5">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'role' => 'form',
        )
    ));
    ?>

    <? echo CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'name', array('for' => '')); ?>
        <?=$form->textField($model, 'name', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'image', array('for' => 'image')); ?>
        <?
        $this->widget('admin.widgets.FileUploaderWidget',
            array(
                'model' => $model,
                'attribute' => 'image',
                'htmlOptions' => array(
                    'data-show-upload' => 'false',
                    'multiple' => false,
                    'class' => 'file'
                ),
            )
        );
        ?>
    </div>

    <div class="form-group">
        <? if(!empty($model->image)): ?>
            <?=$model->getThumbHtml(ImageModelBehavior::SIZE_PREFIX_TINY); ?>
        <? endif; ?>
    </div>

    <?=YHtml::saveButton(); ?>
    <?=YHtml::cancelButton(); ?>

    <? $this->endWidget(); ?>
</div>

