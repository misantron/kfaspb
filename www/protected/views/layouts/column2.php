<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=CHtml::encode(Yii::app()->name . ' - ' . $this->getPageTitle());?></title>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans&subset=cyrillic' rel='stylesheet' type='text/css'>

    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <link type="text/css" rel="stylesheet" href="/css/main.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div id="header-wrapper">
    <div id="header-top">
        <div class="container">
            <div id="header-title">
                Коллегия футбольных арбитров Санкт-Петербурга
            </div>
        </div>
    </div>
    <div class="outline"></div>
    <div id="header-menu-wrapper">
        <div class="container">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand" title="На главную" id="header-menu-logo"></a>
                </div>
                <div id="main-nav" class="navbar-collapse collapse">
                    <?php $this->widget('zii.widgets.CMenu', array(
                        'items' => array(
                            array('label'=>'Руководство', 'url'=>array('/management')),
                            array('label'=>'Документы', 'url'=>array('/file/list')),
                            array('label'=>'Правила игры', 'url'=>array('/law')),
                            array('label'=>'Арбитры', 'url'=>array('/referee/list')),
                            array('label'=>'Бригады', 'url'=>array('/brigade/list')),
                            array('label'=>'Стадионы', 'url'=>array('/stadium/list')),
                            array('label'=>'Контакты', 'url'=>array('/contact')),
                        ),
                        'htmlOptions' => array(
                            'class' => 'nav navbar-nav'
                        )
                    )); ?>
                </div>
            </nav>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div id="main-wrapper" class="container">
    <div class="col-sm-9">
        <div class="main-content">
            <?php echo $content; ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="sidebar-content">

            <?php $this->widget('application.widgets.InfoWidget'); ?>
            <?php $this->widget('application.widgets.BirthdayWidget'); ?>
            <?php $this->widget('application.widgets.LinksWidget'); ?>

        </div>
    </div>
</div>
<div id="footer-wrapper">
    <div class="outline"></div>
    <div id="footer-info-wrap">
        <div class="footer-info container">
            <div class="col-sm-3">
                <div class="footer-block-content">
                    196084, г. Санкт-Петербург,<br />
                    Московский пр., д. 148-Д, 5 этаж<br /><br />
                    +7 (812) 368-29-85<br /><br />
                    <a href="mailto:kfaspb@mail.ru">kfaspb@mail.ru</a>
                    <br />
                    <a href="http://vk.com/kfa_spb" target="_blank">http://vk.com/kfa_spb</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="footer-block-content">
                    Основные цели работы коллегии – развитие судейства и инспектирования, реализация федеральных и региональных программ по развитию судейства и инспектирования соревнований по футболу под эгидой Федерации футбола Санкт-Петербурга, МРО "Северо-Запад", РФС и ФИФА.
                </div>
            </div>
            <div class="col-sm-5">
                <div class="footer-block-content text-right">
                    <a href="http://www.rfs.ru" class="footer-block-organization rfs" target="_blank"></a>
                    <a href="http://mronw.ru" class="footer-block-organization mro-nw" target="_blank"></a>
                    <a href="http://ff.spb.ru" class="footer-block-organization ff-spb" target="_blank"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="footer-nav-wrap">
    <div class="container">
        <div class="col-sm-9">
            <?php $this->widget('zii.widgets.CMenu', array(
                'items' => array(
                    array('label'=>'Главная', 'url'=> '/'),
                    array('label'=>'Руководство', 'url'=>array('/management')),
                    array('label'=>'Документы', 'url'=>array('/file/list')),
                    array('label'=>'Правила игры', 'url'=>array('/law')),
                    array('label'=>'Арбитры', 'url'=>array('/referee/list')),
                    array('label'=>'Бригады', 'url'=>array('/brigade/list')),
                    array('label'=>'Стадионы', 'url'=>array('/stadium/list')),
                    array('label'=>'Контакты', 'url'=>array('/contact')),
                ),
                'htmlOptions' => array(
                    'id' => 'footer-nav-links',
                ),
            )); ?>
            <div id="footer-copyright">
                © 2005-<?=date('Y')?> Все права защищены. При использовании материалов ссылка на сайт обязательна.
            </div>
        </div>
        <div class="col-sm-3 text-right">
            <a class="footer-social-link" href="http://vk.com/kfa_spb" target="_blank">
                <i class="fa fa-vk"></i>
                <span>Группа ВКонтакте</span>
            </a>
        </div>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>