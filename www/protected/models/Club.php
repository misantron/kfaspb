<?php

/**
 * This is the model class for table "team".
 *
 * @property string $stadium_id
 * @property string $name
 * @property string|array $teams
 * @property string $image
 */
class Club extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'club';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_visible', 'numerical', 'integerOnly'=>true),
			array('stadium_id', 'length', 'max'=>11),
			array('name, image', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, stadium_id, name, teams, is_visible, image, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'stadium' => array(self::BELONGS_TO, 'Stadium', 'stadium_id')
		);
	}

    public function behaviors()
    {
        return array(
            'defaultBehavior' => array(
                'class' => 'DefaultModelBehavior',
            ),
            'fileBehavior' => array(
                'class' => 'ImageModelBehavior',
                'baseDirectory' => 'data.club',
            ),
        );
    }

    public function setAttributes($values,$safeOnly=true)
    {
        parent::setAttributes($values,$safeOnly);

        $this->teams = CJSON::encode($values['teams']);
    }

    public function afterFind()
    {
        $this->teams = CJSON::decode($this->teams, true);
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'team_id' => '#',
			'stadium_id' => 'Стадион',
			'name' => 'Название',
			'teams' => 'Команды',
			'is_visible' => 'Видимость',
			'image' => 'Логотип',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('stadium_id',$this->stadium_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('teams',$this->teams,true);
		$criteria->compare('is_visible',$this->is_visible);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Team the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
