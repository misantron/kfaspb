<?php

class FileController extends AdminController
{
    protected $_modelClass = 'File';
    public $title = 'Документы';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
            ),
            'create' => array(
                'class' => 'CreateAction',
            ),
            'edit' => array(
                'class' => 'EditAction',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
            'visibility' => array(
                'class' => 'VisibilityAction',
            ),
        );
    }
}