<?php

class AjaxResponse
{
    protected $_result;
    protected $_message;
    protected $_errors;
    protected $_data;

    public function __construct($result = false)
    {
        $this->_result = $result;
        $this->_errors = array();
    }

    public function setResult($value)
    {
        $this->_result = (bool)$value;
        return $this;
    }

    public function addError($value)
    {
        $this->_errors[] = $value;
        return $this;
    }

    public function setErrors($value)
    {
        $this->_errors = $value;
        return $this;
    }

    public function setMessage($value)
    {
        $this->_message = $value;
        return $this;
    }

    public function setData($value)
    {
        $this->_data = $value;
        return $this;
    }

    public function render()
    {
        echo CJSON::encode(array(
            'result' => $this->_result,
            'message' => $this->_message,
            'errors' => $this->_errors,
            'data' => $this->_data,
        ));
        Yii::app()->end();
    }
}