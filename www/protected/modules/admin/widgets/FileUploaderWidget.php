<?php

class FileUploaderWidget extends CInputWidget
{
    public $options = array();

    public function init()
    {
        list($this->name, $this->id) = $this->resolveNameID();

        $this->options = array(
            'showUpload' => false,
            'maxFileSize' => 2097152,
            'maxFileNum' => 1,
        );

        echo YHtml::activeFileField($this->model, $this->attribute, $this->htmlOptions);

        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        $js = '$("#' . $this->id . '").fileinput(' . CJavaScript::encode($this->options) . ')';

        Yii::app()->clientScript
            ->registerPackage('file-uploader')
            ->registerScript(__CLASS__ . '#' . $this->id, $js, CClientScript::POS_READY);
    }
}