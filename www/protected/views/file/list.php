<?
/**
 * @var array $data
 */
?>
<div class="content-block">
    <h1>Документы</h1>
    <? foreach($data as $item): ?>
        <h2><?=$item['title']?></h2>
        <ul class="file-link-list">
            <? foreach($item['data'] as $file): ?>
            <li>
                <a href="<?=$file->path?>" target="_blank" class="file-link file-link-<?=$file->extension?>">
                    <?=$file->name?>
                </a>
            </li>
            <? endforeach; ?>
        </ul>
    <? endforeach; ?>
</div>