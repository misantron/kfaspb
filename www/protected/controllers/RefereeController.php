<?php

class RefereeController extends BaseController
{
    protected $_modelClass = 'Referee';

    public function actionList()
    {
        $letter = Yii::app()->request->getParam('l', null);
        $modelClass = $this->_modelClass;
        if(!empty($letter)){
            $models = $modelClass::model()->visible()->findAll(array(
                'condition' => 'SUBSTR(last_name,1,1) = :letter',
                'params' => array(':letter' => $letter),
                'order' => 'last_name'
            ));

            $this->render('list', array(
                'models' => $models
            ));
        } else {
            $letters = $modelClass::model()->getActiveCharset();
            $letters = array_chunk($letters, 6);

            $this->render('letters', array(
                'letters' => $letters
            ));
        }
    }

    public function actionProfile()
    {
        $modelClass = $this->_modelClass;
        $id = (int)Yii::app()->request->getParam('id', 0);

        /**
         * @var Referee $model
         */
        $model = $modelClass::model()->visible()->findByPk($id);
        if($model === null){
            $this->returnUrlRedirect();
        }

        $career = array();
        if(is_array($model->career)){
            foreach($model->career as $row){
                if(!isset($career[$row->division_id])){
                    $career[$row->division_id] = array();
                }
                $career[$row->division_id][$row->status] = $row->year;
            }
        }
        $model->career = $career;

        $this->setPageTitle('Арбитр - ' . $model->getFullName());

        $this->render('profile', array(
            'model' => $model,
        ));
    }
}