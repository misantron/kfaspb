<?php

class UserIdentity extends CUserIdentity
{
    private $_id;

	public function authenticate()
	{
        $model = User::model()->findByAttributes(array(
            'email' => strtolower($this->username),
        ));
		if(!$model) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif($model->password !== UtilHelper::getHash($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $model->user_id;

            $this->setState('email', $model->email);
            $this->setState('name', sprintf('%s %s', $model->first_name, $model->last_name));

            $this->errorCode = self::ERROR_NONE;
        }
		return $this->errorCode === self::ERROR_NONE;
	}

    public function getId()
    {
        return $this->_id;
    }
}