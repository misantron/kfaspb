<?php

Yii::import('zii.widgets.grid.CGridView');

/**
 * Class YGridView
 */
class YGridView extends CGridView
{
    public $enableSorting = false;
    public $emptyText = 'Данные отсутствуют';
    public $htmlOptions = array(
        'class' => 'table-responsive'
    );
    public $itemsCssClass = 'table table-striped';
    public $template = '{items} {pager}';
    public $pager = array(
        'class' => 'YLinkPager',
    );
    public $pagerCssClass = 'text-center';
    public $rowCssClassExpression = '$data->isVisible()?"":"text-muted"';
    public $cssFile = false;
    public $ajaxUpdate = false;

    public $buttons = array();
    public $emptyDataCellCssClass = 'text-center';

    public function init()
    {
        if(!empty($this->buttons)){
            $this->columns[] = array(
                'class' => 'YButtonColumn',
                'template' => $this->prepareButtonTemplate()
            );
        }

        parent::init();
    }

    /**
     * @return string
     */
    protected function prepareButtonTemplate()
    {
        $buttons = array();
        foreach ($this->buttons as $value) {
            $buttons[] = sprintf('{%s}', $value);
        }
        return implode(' ', $buttons);
    }

    protected function renderEmptyTableBody()
    {
        echo '<tr><td colspan="'.count($this->columns).'" class="'.$this->emptyDataCellCssClass.'">';
        $this->renderEmptyText();
        echo "</td></tr>\n";
    }

    public function renderTableBody()
    {
        $data=$this->dataProvider->getData();
        $n=count($data);
        echo "<tbody>\n";

        if($n>0)
        {
            for($row=0;$row<$n;++$row)
                $this->renderTableRow($row);
        }
        else
        {
            $this->renderEmptyTableBody();
        }
        echo "</tbody>\n";
    }
}