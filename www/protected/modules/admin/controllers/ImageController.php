<?php

class ImageController extends AdminController
{
    protected $_modelClass = 'Image';
    public $title = 'Изображения';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
                'buttons' => array(
                    YButtonColumn::BUTTON_EDIT,
                    YButtonColumn::BUTTON_VISIBLE,
                    YButtonColumn::BUTTON_HIDE,
                ),
            ),
            'create' => array(
                'class' => 'CreateAction',
            ),
            'edit' => array(
                'class' => 'EditAction',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
            'visibility' => array(
                'class' => 'VisibilityAction',
            ),
        );
    }
}