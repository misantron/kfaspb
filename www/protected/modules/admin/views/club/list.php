<?
/**
 * @var ClubController $this
 * @var CActiveDataProvider $dataProvider
 */
?>
    <div class="row text-right">
        <?=YHtml::createButton($this->createUrl('create'))?>
    </div>
<?
    $this->widget('YGridView', array(
        'dataProvider' => $dataProvider,
        'buttons' => $this->getAction()->buttons,
        'columns' => array(
            array(
                'name' => 'Логотип',
                'type' => 'raw',
                'value' => '$data->getThumbHtml(ImageModelBehavior::SIZE_PREFIX_TINY)',
            ),
            array(
                'name' => 'Название',
                'value' => '$data->name',
            ),
            array(
                'name' => 'stadium_id',
                'value' => '$data->stadium->name',
            ),
        ),
    ));
?>