<div class="content-block">
    <h1>Правила игры в футбол</h1>

    <a href="/files/lotg_2016_2017_en.pdf" target="_blank" class="file-link file-link-pdf">
        Правила игры в футбол, редакция сезона <strong>2016/2017</strong> (оригинал на английском языке)
    </a>
    <br />
    <a href="/files/lotg_2015_2016_ru.pdf" target="_blank" class="file-link file-link-pdf">
        Правила игры в футбол, редакция сезона <strong>2015/2016</strong> (перевод на русский язык)
    </a>
    <br />
    <a href="/files/lotg_2014_2015_ru.pdf" target="_blank" class="file-link file-link-pdf">
        Правила игры в футбол, редакция сезона <strong>2014/2015</strong> (перевод на русский язык)
    </a>
    <br />
    <a href="/files/lotg_2013_2014_ru.pdf" target="_blank" class="file-link file-link-pdf">
        Правила игры в футбол, редакция сезона <strong>2013/2014</strong> (перевод на русский язык)
    </a>
    <a href="/files/lotg_2013_2014_en.pdf" target="_blank" class="file-link file-link-pdf">
        Правила игры в футбол, редакция сезона <strong>2013/2014</strong> (оригинал на английском языке)
    </a>
</div>