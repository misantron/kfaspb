<?php

class StadiumController extends AdminController
{
    protected $_modelClass = 'Stadium';
    public $title = 'Стадионы';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
                'dataProviderConfig' => array(
                    'criteria' => array(
                        'order' => 'name'
                    ),
                ),
            ),
            'create' => array(
                'class' => 'CreateAction',
            ),
            'edit' => array(
                'class' => 'EditAction',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
            'visibility' => array(
                'class' => 'VisibilityAction',
            ),
        );
    }
} 