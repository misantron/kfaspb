<?php

class LinksWidget extends CWidget
{
    public $items = array();

    public function init()
    {
        $models = Link::model()->visible()->findAll();
        $this->items = array_chunk($models, 2);

        $this->render('links');
    }
}