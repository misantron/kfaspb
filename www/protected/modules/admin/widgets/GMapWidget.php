<?php

/**
 * Class GMapWidget
 */
class GMapWidget extends CInputWidget
{
    public $title = 'Расположение';

    protected $mapWidth = '800';
    protected $mapHeight = '600';

    public $latitude;
    public $longitude;
    public $zoom = 16;

    /**
     * @var Stadium
     */
    public $model;
    public $attribute = '';

    protected $defaultOptions = array(
        'latitude' => 59.93331766477165,
        'longitude' => 30.3316650390625,
        'zoom' => 12,
    );

    public function init()
    {
        list($this->name, $this->id) = $this->resolveNameID();

        if (!empty($this->htmlOptions)) {
            $options = '';
            foreach ($this->htmlOptions as $key => $value) {
                $options .= $key . '="' . $value . '" ';
            }
            $this->htmlOptions = $options;
        }

        $this->latitude = array(
            'name' => $this->id . '[lat]',
            'value' => $this->model->lat != 0 ? $this->model->lat : $this->defaultOptions['latitude'],
        );
        $this->longitude = array(
            'name' => $this->id . '[lon]',
            'value' => $this->model->lon != 0 ? $this->model->lon : $this->defaultOptions['longitude'],
        );

        if($this->model->getIsNewRecord()){
            $this->zoom = $this->defaultOptions['zoom'];
        }
    }

    public function run()
    {
        $this->registerScripts();

        $this->render('map');
    }

    protected function registerScripts()
    {
        $mapSize = '.gllpMap { width: ' . $this->mapWidth . 'px; height: ' . $this->mapHeight . 'px; }';

        Yii::app()->clientScript
                  ->registerCss('gllMap', $mapSize)
                  ->registerScriptFile('http://maps.googleapis.com/maps/api/js', CClientScript::POS_END)
                  ->registerPackage('gmaps-picker');
    }
}