<?php
/**
 * @var BirthdayWidget $this
 */
?>
<div class="sidebar-block">
    <div class="sidebar-block-title">Дни рождения</div>
    <div>
        <ul>
            <? foreach($this->items as $date => $models): ?>
            <li class="sidebar-birthday-date">
                <?php echo $this->getDateAddition($models['key'])?>
                <?php echo ViewHelper::getDate($models['date'], false)?>
            </li>
            <? foreach($models['data'] as $item): ?>
            <li class="sidebar-birthday-item media">
                <div class="media-left">
                    <a href="<?php echo $item['profileUrl']?>">
                        <img class="sidebar-birthday-referee-img media-object" src="<?php echo $item['thumbUrl']?>" alt="<?php echo $item['name']?>" />
                    </a>
                </div>
                <div class="media-body">
                    <a href="<?php echo $item['profileUrl']?>" title="<?php echo $item['name']?>">
                        <?php echo $item['name']?>
                    </a>
                    <div class="sidebar-birthday-age">
                        <? echo ViewHelper::getAge($item['age'])?>
                    </div>
                </div>
            </li>
            <? endforeach; ?>
            <? endforeach; ?>
        </ul>
    </div>
</div>