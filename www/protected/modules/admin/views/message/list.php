<?
/**
 * @var AdminController $this
 * @var CActiveDataProvider $dataProvider
 */
$this->widget('YGridView', array(
    'dataProvider' => $dataProvider,
    'buttons' => $this->getAction()->buttons,
    'columns' => array(
        array(
            'name' => 'created',
            'value' => '$data->created',
        ),
        array(
            'name' => 'name',
            'value' => '$data->name',
        ),
        array(
            'name' => 'email',
            'value' => '$data->email',
        ),
        array(
            'name' => 'title',
            'value' => '$data->title',
        ),
    ),
));