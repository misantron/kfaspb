<?
/**
 * @var string $title
 * @var string $brief
 * @var array $attachments
 */
?>
<div class="media">
    <? foreach($attachments as $item): ?>
        <div class="media-left">
            <? if(!empty($item['link'])): ?><a href="<?=$item['link']?>" title="<?=$item['title']?>"><? endif; ?>
            <img class="media-object" src="<?=$item['entityUrl']?>" alt="<?=$item['title']?>" />
            <? if(!empty($item['link'])): ?></a><? endif; ?>
        </div>
    <? endforeach; ?>
    <div class="media-body">
        <? if(!empty($title)): ?>
            <h2><?php echo $title; ?></h2>
        <? endif; ?>
        <?php echo nl2br($brief); ?>
    </div>
</div>