<?php

class BrigadeController extends AdminController
{
    protected $_modelClass = 'Brigade';
    public $title = 'Бригады';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
                'buttons' => array(
                    YButtonColumn::BUTTON_VIEW,
                    YButtonColumn::BUTTON_EDIT,
                    YButtonColumn::BUTTON_VISIBLE,
                    YButtonColumn::BUTTON_HIDE,
                    YButtonColumn::BUTTON_DELETE,
                ),
            ),
            'create' => array(
                'class' => 'CreateAction',
            ),
            'edit' => array(
                'class' => 'EditAction',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
            'visibility' => array(
                'class' => 'VisibilityAction',
            ),
        );
    }

    public function actionView()
    {
        $brigadeId = Yii::app()->request->getParam('id', 0);
        /**
         * @var Brigade $model
         */
        $model = Brigade::model()->findByPk($brigadeId);
        if($model === null){
            $this->setErrorMessage(Yii::t('', 'Объект не найден'));
            $this->returnUrlRedirect();
        }

        $this->setPageTitle('Арбитры - ' . $model->name);

        $dataProvider = new CActiveDataProvider('Referee', array(
            'criteria' => array(
                'condition' => 'brigade_id = :id',
                'params' => array(':id' => (int)$brigadeId),
                'order' => 'last_name',
            ),
            'pagination' => array(
                'pageSize' => $this->getPageSize(),
            ),
        ));

        $this->render('view', array(
            'dataProvider' => $dataProvider,
        ));
    }
}