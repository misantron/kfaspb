<?
/**
 * @var BaseController $this
 * @var CActiveForm $form
 * @var Message $model
 */
?>
<div class="content-block col-sm-12">
    <h1>Контакты</h1>
    <ul style="margin-bottom: 20px">
        <li>
            <strong>Адрес:</strong> 196084, г. Санкт-Петербург, Московский пр., д. 148-Д (5 этаж),
        </li>
        <li>
            <strong>Телефон:</strong> +7 (812) 368-29-85,
        </li>
        <li>
            <strong>Электронная почта:</strong> <a href="mailto:kfaspb@mail.ru">kfaspb@mail.ru</a>
        </li>
    </ul>
    <?
    $this->widget('application.widgets.GoogleMapWidget', array(
        'markers' => array(
            array(
                'title' => 'Коллегия футбольных арбитров<br />196084, г. Санкт-Петербург, Московский пр., д. 148-Д (5 этаж)<br />+7 (812) 368-29-85',
                'position' => array(
                    'lat' => '59.885701',
                    'lon' => '30.322109',
                ),
            ),
        ),
        'zoom' => 16,
    ));
    ?>
    <h2>Написать сообщение</h2>
    <div class="contact-form-wrapper col-sm-8">
    <?
    $form = $this->beginWidget('CActiveForm', array(
        'id' => $this->getFormId(),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array(
            'role' => 'form',
            'class' => 'form',
        )
    ));
    ?>

    <? foreach($this->getMessages() as $type => $message): ?>
        <div class="alert alert-<?=$type?>" role="alert"><?=$message?></div>
    <? endforeach; ?>

    <? echo CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'name', array('for' => 'name')); ?>
        <?=$form->textField($model, 'name', array('class' => 'form-control')); ?>
    </div>
    <div class="form-group">
        <?=$form->labelEx($model, 'email', array('for' => 'email')); ?>
        <?=$form->textField($model, 'email', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'title', array('for' => 'title')); ?>
        <?=$form->textField($model, 'title', array('class' => 'form-control')); ?>
    </div>
    <div class="form-group">
        <?=$form->labelEx($model, 'body', array('for' => 'body')); ?>
        <?=$form->textArea($model, 'body', array('class' => 'form-control', 'rows' => 6)); ?>
    </div>

    <?=CHtml::submitButton('Отправить', array('class' => 'btn btn-primary'));; ?>

    <? $this->endWidget(); ?>
    </div>
</div>