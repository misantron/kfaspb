<?
    /**
     * @var BrigadeController $this
     * @var CActiveDataProvider $dataProvider
     */
?>
    <div class="row text-right">
        <?=YHtml::createButton($this->createUrl('create'))?>
    </div>
<?
    $this->widget('YGridView', array(
        'dataProvider' => $dataProvider,
        'buttons' => $this->getAction()->buttons,
        'columns' => array(
            array(
                'name' => 'Название',
                'value' => '$data->name',
            ),
            array(
                'name' => 'Количество арбитров',
                'type' => 'raw',
                'value' => '$data->refereeCount',
            ),
        ),
    ));
?>