<?php

class GoogleMapWidget extends CWidget
{
    public $canvas = 'map-canvas';
    public $markers = array();
    public $zoom;

    public function run()
    {
        $this->registerScripts();

        echo CHtml::tag('div', array('id' => $this->canvas), '');
    }

    protected function registerScripts()
    {
        $js = "$('#".$this->canvas."').gmap().bind('init', function(ev, map) {"
	                .$this->renderMarkersScript()
                    ."$('#".$this->canvas."').gmap('fitBounds');"
                    .($this->zoom ? "$('#".$this->canvas."').gmap('option', 'zoom', ".$this->zoom.");" : "")."
                });";

        Yii::app()->clientScript
            ->registerScriptFile('http://maps.googleapis.com/maps/api/js', CClientScript::POS_END)
            ->registerScriptFile('/js/jquery.map.ui.js', CClientScript::POS_END)
            ->registerScript(__CLASS__ . $this->getId(), $js, CClientScript::POS_READY);
    }

    protected function renderMarkersScript()
    {
        $js = '';
        foreach ($this->markers as $item) {
            $js .= "$('#".$this->canvas."').gmap('addMarker', {'position': new google.maps.LatLng(".$item['position']['lat'].",".$item['position']['lon']."), 'bounds': true}).click(function() {
                        $('#".$this->canvas."').gmap('openInfoWindow', {'content': '".$item['title']."'}, this);
	                });";
        }
        return $js;
    }
}