<?php

class RefereeWidget extends CWidget
{
    public $itemId;
    public $thumbSize = ImageModelBehavior::SIZE_PREFIX_MEDIUM;
    /** @var Referee */
    public $model;

    public function init()
    {
        $this->model = Referee::model()->visible()->findByPk($this->itemId);
        if($this->model === null){
            echo 'Invalid param id';
            return;
        }

        $this->render('referee');
    }
}