<?
/**
 * @var $this AdminController
 * @var $form YActiveForm
 * @var $model Referee
 */
?>
<div class="col-sm-10">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'role' => 'form',
            'class' => 'form-horizontal',
        )
    ));
    ?>

    <? echo CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'image', array('for' => 'image', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?
            $this->widget('admin.widgets.FileUploaderWidget',
                array(
                    'model' => $model,
                    'attribute' => 'image',
                    'htmlOptions' => array(
                        'data-show-upload' => 'false',
                        'multiple' => false,
                        'class' => 'file'
                    ),
                )
            );
            ?>
        </div>
    </div>

    <? if(!empty($model->id)): ?>
        <div class="form-group">
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <?=$model->getImageHtml(); ?>
            </div>
        </div>
    <? endif; ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'last_name', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'last_name', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'first_name', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'first_name', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'patronymic', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'patronymic', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group"></div>

    <div class="form-group">
        <?=$form->labelEx($model, 'category', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'category', DictHelper::getList('category'), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'category_get_year', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'category_get_year', UtilHelper::getYearRange(), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'referee_start_year', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'referee_start_year', UtilHelper::getYearRange(), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'college_enter_year', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'college_enter_year', UtilHelper::getYearRange(), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'brigade_id', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'brigade_id', YHtml::listData(Brigade::model()->visible()->findAll(), 'id', 'name'), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'brigade_status', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'brigade_status', DictHelper::getList('brigade_status'), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group"></div>

    <div class="form-group">
        <?=$form->labelEx($model, 'birth_date', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?php $this->widget('admin.widgets.DatePickerWidget',
                array(
                    'model' => $model,
                    'attribute' => 'birth_date',
                    'htmlOptions' => array(
                        'class' => 'form-control',
                    ),
                )
            );?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'birth_place', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'birth_place', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'marital', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'marital', DictHelper::getList('marital'), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group"></div>

    <div class="form-group">
        <?=$form->labelEx($model, 'education_level', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->dropDownList($model, 'education_level', DictHelper::getList('education'), array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'education_place', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'education_place', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'highschool_name', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textArea($model, 'highschool_name', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'profession', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'profession', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'language', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'language', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'sports_rank', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'sports_rank', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'hobby', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textArea($model, 'hobby', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'work_place', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'work_place', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'awards', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textArea($model, 'awards', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'achievements', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textArea($model, 'achievements', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'public_work', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textArea($model, 'public_work', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'tournaments', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textArea($model, 'tournaments', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group"></div>

    <div class="form-group">
        <?=$form->labelEx($model, 'email', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'email', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'web', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?=$form->textField($model, 'web', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'is_veteran', array('for' => '', 'class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-3">
            <?=$form->checkBox($model, 'is_veteran'); ?>
        </div>
    </div>

    <? $this->widget('RefereeCareerWidget', array('itemId' => $model->id)); ?>

    <div class="form-group"></div>

    <? $this->widget('RefereeCommissionWidget', array('itemId' => $model->id)); ?>

    <div class="form-group"></div>

    <div class="form-group">
        <div class="col-sm-3"></div>
        <div class="col-sm-9">
            <?=YHtml::saveButton(); ?>
            <?=YHtml::cancelButton(); ?>
        </div>
    </div>

    <? $this->endWidget(); ?>
</div>