<?php

/**
 * Class UtilHelper
 */
class UtilHelper
{
    const SECRET_KEY = '97342215053497a750ced94.27258802';

    const
        FILE_NAME_LENGTH = 10,
        FILE_NAME_SALT = '$JvKnrQ$WPsThu4JteNQA3uH'
    ;

    /**
     * @param $cypher
     * @param $string
     * @return string
     */
    public static function getHash($string, $cypher = 'ripemd256')
    {
        return hash($cypher, $string . self::SECRET_KEY); // len 64
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generatePassword($length = 10)
    {
        $randChars = array();
        $alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $alphabetSize = strlen($alphabet);
        while(--$length+1){
            $randChars[] = $alphabet[mt_rand(0, $alphabetSize-1)];;
        }
        shuffle($randChars);
        return implode('', $randChars);
    }

    /**
     * @return string
     */
    public static function generateFileName()
    {
        $hash = sha1(uniqid(self::FILE_NAME_SALT . mt_rand(), true));
        return substr($hash, 0, self::FILE_NAME_LENGTH);
    }

    /**
     * @return array
     */
    public static function getAlphabet()
    {
        return array(
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й',
            'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф',
            'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'
        );
    }

    /**
     * @return array
     */
    public static function getYearRange()
    {
        $range = array();
        foreach(range(date('Y'), 1920) as $year){
            $range[$year] = $year;
        }
        return $range;
    }
}