<?
/**
 * @var AdminController $this
 * @var ApActiveDataProvider $dataProvider
 */
?>
<div class="row text-right">
    <?=YHtml::createButton($this->createUrl('create'))?>
</div>
<?
$this->widget('admin.extensions.alphapager.ApGridView', array(
    'dataProvider' => $dataProvider,
    'buttons' => $this->getAction()->buttons,
    'template'=>'{alphapager} {items} {pager}',
    'columns' => array(
        array(
            'name' => 'Фамилия / Имя / Отчество',
            'value' => '$data->fullName'
        ),
        array(
            'name' => 'Дата рождения',
            'value' => '$data->birth_date',
        ),
        array(
            'name' => 'Категория',
            'value' => 'DictHelper::getById(\'category\', $data->category)',
        ),
        array(
            'name' => 'Бригада',
            'value' => '$data->brigade_id',
        ),
    ),
));
?>