<?php

class AdminModule extends CWebModule
{
    public $defaultController = 'news';

    protected $_assetsDirectory;
    protected $_packages;

    public function init()
    {
        parent::init();

        $this->importComponents();
        $this->addPackages();
        $this->registerClientScripts();
    }

    protected function getAssetsDirectory()
    {
        if($this->_assetsDirectory === null){
            $path = Yii::getPathOfAlias($this->getId() . '.assets');
            $this->_assetsDirectory = Yii::app()->getAssetManager()->publish($path, false, -1, YII_DEBUG);
        }
        return $this->_assetsDirectory;
    }

    protected function importComponents()
    {
        $moduleId = $this->getId();
        $this->setImport(array(
            $moduleId . '.components.*',
            $moduleId . '.components.widgets.*',
            $moduleId . '.helpers.*',
            $moduleId . '.extensions.alphapager.*',
            $moduleId . '.widgets.*',
        ));
        Yii::app()->setComponents(array(
            'errorHandler' => array(
                'errorAction'=> $moduleId . '/system/error',
            ),
        ));
    }

    protected function addPackages()
    {
        $assetsDirectory = $this->getAssetsDirectory();
        $clientScript = Yii::app()->getClientScript();
        $packages = $this->getPackages();

        foreach ($packages as $name => $config) {
            $config['baseUrl'] = $assetsDirectory;
            $clientScript->addPackage($name, $config);
        }
    }

    protected function getPackages()
    {
        if($this->_packages === null){
            $this->_packages = array(
                'date-picker' => array(
                    'js' => array('js/date-picker.js'),
                    'css' => array('css/date-picker.css'),
                    'depends' => array('bootstrap'),
                ),
                'file-uploader' => array(
                    'js' => array('js/file-uploader.js'),
                    'css' => array('css/file-uploader.css'),
                    'depends' => array('bootstrap'),
                ),
                'typeahead' => array(
                    'js' => array('js/typeahead.js'),
                    'depends' => array('bootstrap'),
                ),
                'main' => array(
                    'css' => array('css/main.css'),
                    'depends' => array('bootstrap'),
                ),
                'login' => array(
                    'css' => array('css/login.css'),
                    'depends' => array('bootstrap'),
                ),
                'gmaps-picker' => array(
                    'js' => array('js/gmaps-picker.js'),
                    'depends' => array('jquery'),
                ),
                'summer-note' => array(
                    'js' => array('js/summer-note.js', 'js/lang/summer-note-ru-RU.js'),
                    'css' => array('css/summer-note.css', 'css/font-awesome.css'),
                    'depends' => array('bootstrap'),
                ),
            );
        }
        return $this->_packages;
    }

    protected function registerClientScripts()
    {

    }
}