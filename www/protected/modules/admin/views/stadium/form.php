<?
/**
 * @var $this AdminController
 * @var $form YActiveForm
 * @var $model Stadium
 */
?>
<div class="col-sm-5">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
    ));
    ?>

    <? echo CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'name', array('for' => '')); ?>
        <?=$form->textField($model, 'name', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'address', array('for' => '')); ?>
        <?=$form->textField($model, 'address', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'field_type', array('for' => '')); ?>
        <?=$form->dropDownList($model, 'field_type', DictHelper::getList('field_type'), array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'field_size', array('for' => '')); ?>
        <?=$form->textArea($model, 'field_size', array('class' => 'form-control')); ?>
    </div>

    <? $this->widget('admin.widgets.GMapWidget', array(
        'model' => $model
    )); ?>

    <?=YHtml::saveButton(); ?>
    <?=YHtml::cancelButton(); ?>

    <? $this->endWidget(); ?>
</div>