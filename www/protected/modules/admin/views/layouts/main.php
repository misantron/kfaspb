<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../../favicon.ico">
    <title><?=CHtml::encode(Yii::app()->name . ' | ' . $this->getPageTitle());?></title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><?=CHtml::encode(Yii::app()->name);?></a>
        </div>
        <div class="navbar-collapse collapse">
            <?php $this->widget('zii.widgets.CMenu', array(
                'encodeLabel' => false,
                'htmlOptions' => array(
                    'class' => 'nav navbar-nav navbar-right',
                ),
                'items'=>array(
                    array('label'=>'Арбитры', 'url'=>array('/admin/referee/list')),
                    array('label'=>'Бригады', 'url'=>array('/admin/brigade/list')),
                    array('label'=>'Стадионы', 'url'=>array('/admin/stadium/list')),
                    array('label'=>'Клубы', 'url'=>array('/admin/club/list')),
                    array('label'=>'Новости', 'url'=>array('/admin/news/list')),
                    array('label'=>'Изображения', 'url'=>array('/admin/image/list')),
                    array('label'=>'Документы', 'url'=>array('/admin/file/list')),
                    array('label'=>'Ссылки', 'url'=>array('/admin/link/list')),
                    array('label'=>'Сообщения', 'url'=>array('/admin/message/list')),
                    array('label'=>'<i class="glyphicon glyphicon-user"></i>&nbsp;('.Yii::app()->user->email.')', 'url'=>array('/admin/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
                ),
            )); ?>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">

        <div class="col-sm-3 col-md-2 sidebar">
            <?php $this->widget('admin.components.widgets.YMenu', array(
                'items'=>array(
                    array(
                        'label'=>'<i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Арбитры',
                        'url'=>array('/admin/referee/list')),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;Бригады',
                        'url'=>array('/admin/brigade/list')),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-map-marker"></i>&nbsp;&nbsp;Стадионы',
                        'url'=>array('/admin/stadium/list')),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;Клубы',
                        'url'=>array('/admin/club/list')),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-signal"></i>&nbsp;&nbsp;Новости',
                        'url'=>array('/admin/news/list')),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-picture"></i>&nbsp;&nbsp;Изображения',
                        'url'=>array('/admin/image/list')),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;Документы',
                        'url'=>array('/admin/file/list')
                    ),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-link"></i>&nbsp;&nbsp;Ссылки',
                        'url'=>array('/admin/link/list')
                    ),
                    array(
                        'label'=>'<i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;Сообщения',
                        'url'=>array('/admin/message/list')
                    ),
                ),
            )); ?>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"><?=CHtml::encode($this->getPageTitle());?></h1>

            <? foreach($this->getMessages() as $type => $message): ?>
                <div class="alert alert-<?=$type?>" role="alert"><?=$message?></div>
            <? endforeach; ?>

            <?=$content;?>
        </div>

    </div>
</div>

</body>
</html>
