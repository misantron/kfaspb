<?
/**
 * @var $this AdminController
 * @var $form YActiveForm
 * @var $model Club
 */
?>
<div class="col-sm-5">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'role' => 'form',
        )
    ));
    ?>

    <? echo CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'name', array('for' => '')); ?>
        <?=$form->textField($model, 'name', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'stadium_id', array('for' => '')); ?>
        <?=$form->dropDownList($model, 'stadium_id', Stadium::model()->getArrayDataList(), array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'image', array('for' => '')); ?>
        <?
        $this->widget('admin.widgets.FileUploaderWidget',
            array(
                'model' => $model,
                'attribute' => 'image',
                'htmlOptions' => array(
                    'data-show-upload' => 'false',
                    'multiple' => false,
                    'class' => 'file'
                ),
            )
        );
        ?>
    </div>

    <? if(!empty($model->id)): ?>
        <div class="form-group">
            <? echo $model->getThumbHtml(ImageModelBehavior::SIZE_PREFIX_SMALL); ?>
        </div>
    <? endif; ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'teams', array('for' => 'teams')); ?>
        <div>
            <?=$form->checkBoxList($model, 'teams', DictHelper::getList('teams')); ?>
        </div>
    </div>

    <?=YHtml::saveButton(); ?>
    <?=YHtml::cancelButton(); ?>

    <? $this->endWidget(); ?>
</div>