<?php

class YActiveForm extends CActiveForm
{
    public $enableAjaxValidation = true;
    public $enableClientValidation = true;

    public $clientOptions = array(
        'validateOnSubmit' => true,
    );

    public $htmlOptions = array(
        'role' => 'form'
    );
}