<?php

class NewsAttachmentWidget extends CInputWidget
{
    public $items = array();
    public $itemId;

    protected $modelClass;

    public function init()
    {
        $this->modelClass = 'NewsAttachment';

        $imageList = Image::model()->visible()->findAll();

        $criteria = new CDbCriteria(array(
            'condition' => 'news_id = :id',
            'params' => array(':id' => $this->itemId),
        ));
        $this->items = NewsAttachment::model()->findAll($criteria);
        $additionalCount = 2 - sizeof($this->items);

        for($i=0;$i<$additionalCount;$i++){
            $this->items[] = new NewsAttachment();
        }

        $this->render('news_attachment', array(
            'imageList' => $imageList,
        ));

        $this->registerClientScripts();
    }

    protected function registerClientScripts()
    {
        $js = 'var data = [], refereeSearchKey = null;' . "\n";

        $js .= '$(".type-selector").change(function(){
            var key = $(this).attr("data-key"), value = $(this).val();
            switch(value){
                case "0":
                    $(".img-selector-" + key, ".referee-selector-" + key).addClass("hidden");
                    break;
                case "1":
                    $(".img-selector-" + key).removeClass("hidden");
                    $(".referee-selector-" + key).addClass("hidden");
                    break;
                case "2":
                    $(".img-selector-" + key).addClass("hidden");
                    $(".referee-selector-" + key).removeClass("hidden");
                    break;
            }
        });' . "\n";

        $js .= '$(".thumbnail img").click(function(){
            var key = $(this).attr("data-key");
            $("#' . $this->modelClass . '_" + key + "_entity_id").val($(this).attr("data-id"));
            $(".image-select-" + key).modal("hide");
        });' . "\n";

        $js .= '$(".referee-search").typeahead({
            minLength: 3,
            source: function(query, process){
                return $.ajax({
                    url: "/admin/referee/search",
                    type: "POST",
                    data: {pattern: query},
                    dataType: "json",
                    success: function(response, textStatus){
                        var names = [];
                        $.each(response.data, function(i, value){
                            names[i] = value.name;
                        });
                        data = response.data;
                        return process(names);
                    },
                    error: function(){
                        console.log("Server internal error");
                        return;
                    }
                });
            },
            matcher: function(item){
                if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                    return true;
                }
            },
            afterSelect: function(item){
                var key;
                $.each(data, function(i, value){
                    if(item == value.name){
                        key = i;
                        return;
                    }
                });
                $("#' . $this->modelClass . '_" + refereeSearchKey + "_entity_id").val(data[key].id);
                $("#' . $this->modelClass . '_" + refereeSearchKey + "_link").val(data[key].url);
                $(".referee-select-" + refereeSearchKey).modal("hide");
            }
        }).click(function(){
            refereeSearchKey = $(this).attr("data-key");
        });';

        Yii::app()->clientScript
            ->registerPackage('typeahead')
            ->registerScript(__CLASS__ . '#' . $this->id, $js, CClientScript::POS_READY);
    }
}