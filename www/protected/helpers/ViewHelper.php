<?php

class ViewHelper
{
    public static $encoding = 'utf-8';

    /**
     * @param int $n
     * @param array $forms
     * @return string
     */
    public static function getPluralForm($n, $forms)
    {
        return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
    }

    /**
     * @param int $age
     * @return string
     */
    public static function getAge($age)
    {
        if($age < 1) return '';
        return sprintf('%d %s', $age, self::getPluralForm($age, array('год', 'года', 'лет')));
    }

    /**
     * @param int $timestamp
     * @param bool $withYear
     * @return string
     */
    public static function getDate($timestamp, $withYear = true)
    {
        $timestamp = (int)$timestamp;
        if(!$timestamp){
            return '';
        }
        $months = array(
            'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
            'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',
        );
        $date = date('j', $timestamp) . ' ' . $months[date('n', $timestamp)-1];
        if($withYear) $date .= ' ' . date('Y', $timestamp) . ' года';

        return $date;
    }
}