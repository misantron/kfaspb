<?
/**
 * @var Brigade $model
 */
$count = 1;
?>
<div class="content-block">
    <h1>Состав бригады #<?=$model->id?></h1>
    <div class="brigade-referee-list">
        <? foreach($model->referee as $item): ?>
            <div class="row">
            <? /* @var Referee $item */ ?>
                <div class="col-sm-1" style="margin-top: 10px">
                    #<?=$count?>
                </div>
                <div class="col-sm-6 media">
                    <div class="media-left">
                        <a href="<?php echo $item->getProfileUrl(); ?>" title="<?php echo $item->getFullName()?>">
                            <img class="media-object" width="80px" src="<?php echo $item->getThumbUrl(ImageModelBehavior::SIZE_PREFIX_SMALL) ?>" alt="<?php echo $item->getFullName()?>" />
                        </a>
                    </div>
                    <div class="media-body">
                        <a class="media-heading" href="<?php echo $item->getProfileUrl(); ?>" title="<?php echo $item->getFullName()?>">
                            <?php echo $item->getFullName()?>
                        </a>
                        <p>
                            <?php if(strtotime($item->birth_date) > 0): ?>
                            Дата рождения: <?php echo ViewHelper::getDate(strtotime($item->birth_date)); ?><br />
                            <?php endif; ?>
                            <?php if(!empty($item->category)): ?>
                            <?php echo DictHelper::getById('category', $item->category)?>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-5" style="margin-top: 10px">
                    <?php echo DictHelper::getById('brigade_status', $item->brigade_status)?>
                </div>
            <? $count++; ?>
            </div>
        <? endforeach; ?>
    </div>
</div>