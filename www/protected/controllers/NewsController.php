<?php

class NewsController extends BaseController
{
    protected $_modelClass = 'News';

    public function actionView()
    {
        $modelClass = $this->_modelClass;
        $id = (int)Yii::app()->request->getParam('id', 0);

        $model = $modelClass::model()->visible()->findByPk($id);
        if($model === null){
            $this->redirect('/');
        }

        $this->setPageTitle('Новости');

        $this->render('view', array(
            'model' => $model,
        ));
    }
}