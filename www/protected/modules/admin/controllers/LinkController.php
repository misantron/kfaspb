<?php

class LinkController extends AdminController
{
    protected $_modelClass = 'Link';
    public $title = 'Ссылки';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
            ),
            'create' => array(
                'class' => 'CreateAction',
            ),
            'edit' => array(
                'class' => 'EditAction',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
            'visibility' => array(
                'class' => 'VisibilityAction',
            ),
        );
    }
}