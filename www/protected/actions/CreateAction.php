<?php

class CreateAction extends BaseAction
{
    public $view = 'form';
    public $actionTitle = 'добавить';

    public function run()
    {
        /**
         * @var AdminController $controller
         * @var BaseModel $model
         */
        $controller = $this->getController();
        $modelClass = $controller->getModelClass();
        $childModelClasses = $controller->getChildModelClasses();

        $controller->setPageTitle($controller->title . ' - ' . $this->actionTitle);

        $model = new $modelClass;
        $model->setScenario(BaseModel::ACTION_INSERT);

        $controller->ajaxValidation($model);

        if($data = Yii::app()->request->getParam($modelClass, null)){
            foreach($childModelClasses as $className){
                $data['_relations'][$className] = Yii::app()->request->getParam($className, null);
            }
            $model->setAttributes($data);
            if($model->validate()){
                if($model->save(false)){
                    $controller->setSuccessMessage(Yii::t('', 'Объект успешно добавлен'));
                } else {
                    $controller->setErrorMessage(Yii::t('', 'Ошибка при добавлении объекта'));
                }
                $controller->returnUrlRedirect();
            }
        }

        $controller->render($this->view, array(
            'model' => $model,
        ));
    }
}