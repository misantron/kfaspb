<?
/**
 * @var $this AdminController
 * @var $form YActiveForm
 * @var $model Brigade
 */
?>
<div class="col-sm-5">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
    ));
    ?>

    <?=CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>

    <div class="form-group">
        <?=$form->labelEx($model, 'name', array('for' => 'name')); ?>
        <?=$form->textField($model, 'name', array('class' => 'form-control')); ?>
    </div>

    <?=YHtml::saveButton(); ?>
    <?=YHtml::cancelButton(); ?>

    <? $this->endWidget(); ?>
</div>