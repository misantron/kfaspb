<?php

class RefereeCommissionWidget extends CInputWidget
{
    public $itemId;
    public $items;

    public function init()
    {
        $criteria = new CDbCriteria(array(
            'condition' => 'referee_id = :id',
            'params' => array(':id' => $this->itemId),
        ));
        $this->items = RefereeCommission::model()->findAll($criteria);

        $this->render('referee_commission');
    }
}