<?php

class BirthdayWidget extends CWidget
{
    public $items = array();
    public $period;

    public function init()
    {
        $this->period = 7;

        $criteria = new CDbCriteria(array(
            'condition' => 'is_visible = 1 AND DATE_ADD(birth_date, INTERVAL YEAR(CURDATE())-YEAR(birth_date) YEAR) BETWEEN DATE_ADD(CURDATE(), INTERVAL :start DAY) AND DATE_ADD(CURDATE(), INTERVAL :finish DAY)',
            'params' => array(
                ':start' => '-1',
                ':finish' => $this->period - 1,
            ),
        ));

        $models = Referee::model()->visible()->findAll($criteria);

        foreach ($models as $model) {
            /** @var Referee $model */
            $birthDayTimestamp = strtotime($model->birth_date);
            $key = date('j', $birthDayTimestamp) + date('n', $birthDayTimestamp) * 30;
            if(!isset($this->items[$key])){
                $this->items[$key] = array(
                    'key' => date('dm', $birthDayTimestamp),
                    'date' => $birthDayTimestamp,
                    'data' => array(),
                );
            }
            $age = $model->getAge();
            if(date('Y-m-d', mktime(0, 0, 0, date('m', $birthDayTimestamp), date('d', $birthDayTimestamp), date('Y'))) > date('Y-m-d')) {
                $age += 1;
            }
            $this->items[$key]['data'][] = array(
                'profileUrl' => $model->getProfileUrl(),
                'thumbUrl' => $model->getThumbUrl(),
                'name' => $model->getFullName(),
                'age' => $age
            );
        }

        ksort($this->items);

        $this->render('birthday');
    }

    public function getDateAddition($date)
    {
        if($date == date('dm', mktime(0, 0, 0, date('n'), date('j') - 1))){
            return 'Вчера, ';
        }
        if($date == date('dm')){
            return 'Сегодня, ';
        }
        if($date == date('dm', mktime(0, 0, 0, date('n'), date('j') + 1))){
            return 'Завтра, ';
        }
        return '';
    }
}