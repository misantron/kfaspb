<?
/**
 * @var $this AdminController
 * @var $form YActiveForm
 * @var $model News
 */
?>
<div class="col-sm-6">
    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
    ));
    ?>

    <div class="form-group input-append date">
        <?=$form->labelEx($model, 'published', array('for' => 'published')); ?>
        <?php $this->widget('admin.widgets.DatePickerWidget',
            array(
                'model' => $model,
                'attribute' => 'published',
                'htmlOptions' => array(
                    'class' => 'form-control',
                ),
            )
        );?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'attachments'); ?>
        <? $this->widget('admin.widgets.NewsAttachmentWidget', array('itemId' => $model->id));?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'title', array('for' => '')); ?>
        <?=$form->textField($model, 'title', array('class' => 'form-control')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'brief', array('for' => '')); ?>
        <?=$form->textArea($model, 'brief', array('class' => 'form-control', 'rows' => 5)); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'body', array('for' => 'body')); ?>
        <?php $this->widget('admin.widgets.TextEditorWidget',
            array(
                'model' => $model,
                'attribute' => 'body',
            )
        );?>
    </div>

    <?=YHtml::saveButton(); ?>
    <?=YHtml::cancelButton(); ?>

    <? $this->endWidget(); ?>
</div>