<?php

class FileModelBehavior extends CActiveRecordBehavior
{
    const
        FIELD_EXTENSION = 'extension',
        FIELD_MIME_TYPE = 'mime_type',
        FIELD_BYTE_SIZE = 'byte_size',
        FIELD_FILE_NAME = 'filename',
        FIELD_FILE_PATH = 'path',
        FIELD_FILE_HASH = 'hash'
    ;

    protected $_basePath = 'webroot.';

    protected $_scenarioList = array(
        BaseModel::ACTION_INSERT,
        BaseModel::ACTION_UPDATE
    );

    public $baseDirectory  = 'data.file';
    public $field = 'file';

    protected function saveUploadedFile()
    {
        if($file = CUploadedFile::getInstance($this->owner, $this->field)){
            if(!$this->owner->isNewRecord && !empty($this->owner->{self::FIELD_FILE_PATH})){
                $this->deleteUploadedFile();
            }
            $name = UtilHelper::generateFileName() . '.' . $file->getExtensionName();
            $fileAbsolutePath = $this->getFilePath(true) . $name;

            $this->setModelFields($file, $name);

            if($file->saveAs($fileAbsolutePath)){
                $this->owner->{self::FIELD_FILE_HASH} = md5_file($fileAbsolutePath);
            }
        }
    }

    /**
     * @param CUploadedFile $file
     * @param string $name
     */
    protected function setModelFields(&$file, $name)
    {
        $this->owner->{self::FIELD_EXTENSION} = $file->getExtensionName();
        $this->owner->{self::FIELD_MIME_TYPE} = $file->getType();
        $this->owner->{self::FIELD_BYTE_SIZE} = $file->getSize();
        $this->owner->{self::FIELD_FILE_NAME} = $file->getName();

        $this->owner->{self::FIELD_FILE_PATH} = $this->getFilePath(false) . $name;
    }

    protected function deleteUploadedFile()
    {
        $fileAbsolutePath = Yii::getPathOfAlias(str_replace('.', '', $this->_basePath)) . $this->owner->{self::FIELD_FILE_PATH};
        if(file_exists($fileAbsolutePath)){
            unlink($fileAbsolutePath);
        }
    }

    /**
     * @param bool $absolute
     * @return string
     */
    protected function getFilePath($absolute = false)
    {
        return $absolute ?
            Yii::getPathOfAlias($this->_basePath . $this->baseDirectory) . '/' :
            '/' . str_replace('.', '/', $this->baseDirectory) . '/';
    }

    public function beforeSave($event)
    {
        $this->saveUploadedFile();
    }

    public function afterDelete($event)
    {
        $this->deleteUploadedFile();
    }

    /**
     * @param BaseModel $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);

        if(in_array($owner->scenario, $this->_scenarioList)){
            $fileValidator = CValidator::createValidator('file', $owner, $this->field, $this->getValidationRules());
            $owner->getValidatorList()->add($fileValidator);
        }
    }

    /**
     * @return array
     */
    protected function getValidationRules()
    {
        return array(
            'types' => 'doc,docx,xls,xlsx,txt,zip,pdf,rar',
            'wrongType' => 'Файл "{file}" имеет недопустимый тип. Для загрузки возможны только файлы с расширениями: {extensions}.',
            'maxSize' => 2097152,
            'tooLarge' => 'Файл "{file}" слишком большой. Максимальный размер загружаемого файла - {limit} байт.',
            'allowEmpty' => true,
            'message' => 'Необходимо выбрать файл для загрузки.',
            'maxFiles' => 1,
            'tooMany' => 'Можно загрузить только {limit} файл.',
            'except' => BaseModel::ACTION_VISIBILITY,
        );
    }
}