<?
/**
 * @var array $markers
 * @var array $models
 */
?>
<div class="content-block">
    <h1>Стадионы</h1>

    <? $this->widget('application.widgets.GoogleMapWidget', array('markers' => $markers)); ?>

    <? foreach($models as $model): ?>
        <? /** @var Stadium $model */?>
        <a href="<?=$model->getViewUrl()?>"><?=$model->name?></a><br />
    <? endforeach; ?>
</div>