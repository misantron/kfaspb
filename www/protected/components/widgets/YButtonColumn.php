<?php

Yii::import('zii.widgets.grid.CButtonColumn');

class YButtonColumn extends CButtonColumn
{
    const
        BUTTON_VIEW = 'view',
        BUTTON_EDIT = 'edit',
        BUTTON_VISIBLE = 'visible',
        BUTTON_HIDE = 'hide',
        BUTTON_DELETE = 'delete'
    ;

    public $htmlOptions = array(
        'width' => '1%',
        'nowrap' => 'nowrap',
    );

    public $buttons = array(
        self::BUTTON_VIEW => array(
            'label' => '<i class="glyphicon glyphicon-list"></i>',
            'imageUrl' => false,
            'url' => 'Yii::app()->controller->createUrl("view", array("id" => $data->id))',
            'options' => array(
                'title' => 'Список арбитров',
                'class' => 'btn btn-xs btn-info',
            ),
        ),
        self::BUTTON_EDIT => array(
            'label' => '<i class="glyphicon glyphicon-pencil"></i>',
            'imageUrl' => false,
            'url' => 'Yii::app()->controller->createUrl("edit", array("id" => $data->id))',
            'options' => array(
                'title' => 'Редактировать',
                'class' => 'btn btn-xs btn-primary',
            ),
        ),
        self::BUTTON_VISIBLE => array(
            'label' => '<i class="glyphicon glyphicon-eye-open"></i>',
            'imageUrl' => false,
            'url' => 'Yii::app()->controller->createUrl("visibility", array("id" => $data->id))',
            'visible' => '!$data->is_visible',
            'options' => array(
                'title' => 'Активировать',
                'class' => 'btn btn-xs btn-success',
            ),
        ),
        self::BUTTON_HIDE => array(
            'label' => '<i class="glyphicon glyphicon-eye-close"></i>',
            'imageUrl' => false,
            'url' => 'Yii::app()->controller->createUrl("visibility", array("id" => $data->id))',
            'visible' => '$data->is_visible',
            'options' => array(
                'title' => 'Скрыть',
                'class' => 'btn btn-xs btn-warning',
            ),
        ),
        self::BUTTON_DELETE => array(
            'label' => '<i class="glyphicon glyphicon-remove"></i>',
            'imageUrl' => false,
            'url' => 'Yii::app()->controller->createUrl("delete", array("id" => $data->id))',
            'options' => array(
                'title' => 'Удалить',
                'class' => 'btn btn-xs btn-danger',
            ),
        ),
    );
} 