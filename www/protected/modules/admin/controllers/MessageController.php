<?php

class MessageController extends AdminController
{
    protected $_modelClass = 'Message';
    public $title = 'Сообщения';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
                'buttons' => array(
                    YButtonColumn::BUTTON_VIEW,
                    YButtonColumn::BUTTON_DELETE,
                ),
                'dataProviderConfig' => array(
                    'criteria' => array(
                        'order' => 'created DESC',
                    ),
                ),
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
        );
    }

    public function actionView()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $modelClass = $this->_modelClass;
        /**
         * @var Message $model
         */
        $model = $modelClass::model()->findByPk($id);
        if($model === null){
            $this->setErrorMessage(Yii::t('', 'Объект не найден'));
            $this->returnUrlRedirect();
        }

        $this->setPageTitle('Сообщения - просмотр');

        $this->render('view', array(
            'model' => $model,
        ));
    }
}