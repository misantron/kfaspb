<div class="sidebar-block">
    <div class="sidebar-block-title">Ссылки</div>
    <div class="sidebar-links-items">
        <? foreach($this->items as $chunk): ?>
            <? foreach($chunk as $item): ?>
            <? /* @var Link $item */ ?>
            <a class="sidebar-links-item" title="<?=$item->title?>" target="_blank" href="<?=$item->link?>" style="background: url(<?=$item->getImageUrl()?>) no-repeat"></a>
            <? endforeach; ?>
        <div class="clear"></div>
        <? endforeach; ?>
    </div>
</div>