<div class="sidebar-block">
    <div class="sidebar-block-title">Информация</div>
    <div class="sidebar-important">
        <a href="/protocol/">
            <i class="fa fa-file-text"></i>
            <div>Правила заполнения протокола матча</div>
        </a>
        <a href="/school/">
            <i class="fa fa-futbol-o"></i>
            <div>Как стать арбитром?</div>
        </a>
    </div>
</div>