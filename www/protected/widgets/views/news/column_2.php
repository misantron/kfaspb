<?
/**
 * @var string $title
 * @var string $brief
 * @var array $attachments
 */

$itemLeft = reset($attachments);
$itemRight = end($attachments);
?>
<div class="media">
    <div class="media-left">
        <? if(!empty($itemLeft['link'])): ?><a href="<?=$itemLeft['link']?>" title="<?=$itemLeft['title']?>"><? endif; ?>
        <img class="media-object" src="<?=$itemLeft['entityUrl']?>" alt="<?=$itemLeft['title']?>" />
        <? if(!empty($itemLeft['link'])): ?></a><? endif; ?>
    </div>
    <div class="media-body">
        <? if(!empty($title)): ?>
            <h2><?php echo $title; ?></h2>
        <? endif; ?>
        <?php echo nl2br($brief); ?>
    </div>
    <div class="media-right">
        <? if(!empty($itemRight['link'])): ?><a href="<?=$itemRight['link']?>" title="<?=$itemRight['title']?>"><? endif; ?>
        <img class="media-object" src="<?=$itemRight['entityUrl']?>" alt="<?=$itemRight['title']?>" />
        <? if(!empty($itemRight['link'])): ?></a><? endif; ?>
    </div>
</div>