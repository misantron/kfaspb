<?
/**
 * @var AdminController $this
 * @var CActiveDataProvider $dataProvider
 */
?>
    <div class="row text-right">
        <?=YHtml::createButton($this->createUrl('create'))?>
    </div>
<?
$this->widget('YGridView', array(
    'dataProvider' => $dataProvider,
    'buttons' => $this->getAction()->buttons,
    'columns' => array(
        array(
            'name' => 'image',
            'type' => 'raw',
            'value' => '$data->getImageHtml()',
        ),
        array(
            'name' => 'title',
            'value' => '$data->title',
        ),
        array(
            'name' => 'link',
            'type' => 'raw',
            'value' => 'YHtml::link($data->link, $data->link, array("target" => "_blank"))',
        ),
    ),
));
?>