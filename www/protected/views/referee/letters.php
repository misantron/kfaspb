<?php
/**
 * @var array $letters
 */
?>
<div class="content-block">
    <h1>Арбитры</h1>
    <div class="referee-letters-wrapper">
        <?php foreach($letters as $chunk): ?>
            <div class="row">
                <?php foreach($chunk as $letter): ?>
                    <div class="col-sm-2 referee-list-letter text-center">
                        <a href="/referee/list?l=<?php echo $letter; ?>">
                            <?php echo $letter; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>