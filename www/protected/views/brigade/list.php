<?php
/**
 * @var array $models
 */
?>
<div class="content-block">
    <h1>Бригады</h1>
    <?php foreach($models as $chunk): ?>
    <div class="row">
        <?php foreach($chunk as $model): ?>
        <?php /* @var Brigade $model */ ?>
        <div class="brigade-list-item col-sm-3">
            <a href="<?=$model->getViewUrl()?>" title="Просмотр состава бригады">
                <?=$model->name?>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endforeach; ?>
</div>