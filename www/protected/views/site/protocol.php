<div class="content-block">
    <h1>Правила заполнения протокола матча</h1>

    <a href="/files/protocol_rules_2014.pdf" target="_blank" class="file-link file-link-pdf">
        Правила заполнения бумажной версии протокола матча (2014 год)
    </a>
    <a href="/files/protocol_electron.pdf" target="_blank" class="file-link file-link-pdf">
        Правила заполнения электронной версии протокола матча (2016 год)
    </a>
</div>