<?php

class SiteController extends BaseController
{
    public function actionIndex()
    {
        $this->setPageTitle('Новости коллегии');

        $this->render('index');
    }

    public function actionManagement()
    {
        $this->setPageTitle('Руководство коллегии');

        $this->render('management');
    }

    public function actionLaw()
    {
        $this->setPageTitle('Правила игры');

        $this->render('law');
    }

    public function actionContact()
    {
        $this->setPageTitle('Контакты');
        $modelName = 'Message';

        /**
         * @var Message $model
         */
        $model = new $modelName();
        $model->setScenario(BaseModel::ACTION_INSERT);

        $this->ajaxValidation($model);

        if($data = Yii::app()->request->getParam($modelName, null)){
            $model->setAttributes($data);
            if($model->validate()){
                if($model->save(false)){
                    $this->setSuccessMessage(Yii::t('', 'Сообщение успешно отправлено'));
                } else {
                    $this->setErrorMessage(Yii::t('', 'Ошибка при отправлении'));
                }
                $this->redirect('/contact');
            }
        }

        $this->render('contact', array(
            'model' => $model
        ));
    }

    public function actionProtocol()
    {
        $this->setPageTitle('Правила заполнения протокола');

        $this->render('protocol');
    }

    public function actionSchool()
    {
        $this->setPageTitle('Как стать футбольным арбитром');

        $this->render('school');
    }

    public function actionError()
    {
        if($error = Yii::app()->errorHandler->error) {
            if(Yii::app()->request->isAjaxRequest){
                $response = new AjaxResponse(false);
                $response
                    ->addError($error['message'])
                    ->render();
            }
            $this->setPageTitle('Ошибка ' . $error['code'] . ' - ' . HttpStatusCode::getMessage($error['code']));
            $this->render('error', $error);
        }
    }
}