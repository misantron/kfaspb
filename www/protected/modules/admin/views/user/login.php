<?
/**
 * @var $this UserController
 * @var $form YActiveForm
 * @var $model LoginForm
 * @var $formId string
 */
?>
<div class="container">

    <?
    $form = $this->beginWidget('YActiveForm', array(
        'id' => $this->getFormId(),
        'htmlOptions' => array(
            'class' => 'form-signin',
            'role' => 'form'
        )
    ));
    ?>
        <h2 class="form-signin-heading"><?=CHtml::encode($this->pageTitle)?></h2>

        <div>
            <? echo CHtml::errorSummary($model, 'Пожалуйста, исправьте следующие ошибки:', null, array('class' => 'form-group alert alert-danger')); ?>
        </div>

        <?=$form->textField($model, 'username', array(
            'class' => 'form-control',
            'placeholder' => 'Email',
            'required' => true,
            'autofocus' => true,
        ));
        ?>
        <?=$form->passwordField($model, 'password', array(
            'class' => 'form-control',
            'placeholder' => 'Пароль',
            'required' => true,
        ));
        ?>
        <div class="checkbox">
            <label>
                <?=$form->checkBox($model, 'rememberMe'); ?> Запомнить меня
            </label>
        </div>
        <?=CHtml::htmlButton('Войти', array(
            'type' => 'submit',
            'class' => 'btn btn-lg btn-primary btn-block'
        ));
        ?>
    <? $this->endWidget(); ?>

</div>