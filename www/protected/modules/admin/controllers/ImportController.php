<?php

class ImportController extends AdminController
{
    public $title = 'Импорт';

    public function actionIndex()
    {
        $categories = array(
            '' => 0,
            'судья по спорту' => 1,
            'судья о спорту' => 1,
            '3' => 2,
            '2' => 3,
            '1' => 4,
            'региональная' => 5,
            'национальная' => 6,
            'ФИФА' => 7,
            'ассистент ФИФА' => 8,
            'республиканская' => 9,
            'всесоюзная' => 10,
            'международная' => 11,
        );

        $education = array(
            '' => 0,
            'неполное среднее' => 1,
            'среднее' => 2,
            'среднее профессиональное' => 3,
            'неоконченное высшее' => 4,
            'высшее' => 5,
            'несколько высших' => 6,
            'неоконченное среднее профессиональное' => 7,

            'среднее техническое' => 3,
            'незаконченное высшее' => 4,
            'высшее (инженерное)' => 5,
            'среднее неполное' => 1,
            'среднее специальное' => 3,
            'среднее полное' => 2,
            '2 высших' => 6,
            'среднее полное, незаконченное среднее техническое' => 7,
            'высшее техническое' => 5,
            'незаконченное высшее, среднее специальное' => 4,
            'среднее педагогическое' => 3,
            'незаконченное среднее' => 7,
            'Неполное высшее (ИНЖЭКОН)' => 4,
            'высшее (бакалавр)' => 5,
            'средне-техническое' => 3,
            'неоконченное высшее (МГОУ)' => 4,
            'неполное высшее' => 4,
            'среднее специальное, неоконченное высшее' => 4,
            'Начальное профессиональное (Лицей № 148)' => 3,
            'полное среднее' => 2,
            'Полное среднее' => 2,
            'средне-специальное' => 3,
        );

        $marital = array(
            '' => 0,
            'холост' => 1,
            'женат' => 2,
            'женат, есть ребенок' => 3,
            'женат, есть дети' => 4,
            'разведен' => 5,
            'замужем' => 6,
            'замужем, есть ребенок' => 7,
            'замужем, есть дети' => 8,
            'разведена' => 9,
            'не замужем' => 10,

            'женат, есть дочь' => 3,
            'разведен, дочь 1994 г.р.' => 5,
            'женат, трое детей' => 4,
            'женат, двое детей' => 4,
            'женат, сын' => 3,
            'женат, дочь 2003 г.р.' => 3,
            'женат, сын, дочь' => 4,
            'женат, два сына 1979,1984 г.р.' => 4,
            'положение женат, есть дочка' => 3,
            'не женат' => 1,
            'Холост' => 1,
        );

        $brigadeStatus = DictHelper::getList('brigade_status');
        foreach($brigadeStatus as $k => $val){
            $brigadeStatus[$k] = mb_strtolower($val, 'utf8');
        }
        $brigadeStatus = array_flip($brigadeStatus);

        $list = Yii::app()->db->createCommand()
            ->select()
            ->from('old_ref_data')
            ->queryAll();

        foreach($list as $item) {
            $object = new Referee();
            $object->first_name = $item['name'];
            $object->last_name = $item['surname'];
            $object->patronymic = $item['pat_name'];
            $object->birth_place = $item['born_place'];
            $object->birth_date = $item['born_date'];
            $object->college_enter_year = $item['kfa_enter'];
            $object->referee_start_year = $item['ref_begin'];
            $object->category_get_year = $item['year_kategory'];
            $object->height = $item['height'];
            $object->weight = $item['weight'];
            $object->brigade_id = $item['crew'];
            $object->brigade_status = $brigadeStatus[$item['status_crew']];
            $object->email = $item['e-mail'];
            $object->web = $item['web'];
            $object->awards = $item['award'];
            $object->education_place = $item['learn_place'];
            $object->language = $item['language'];
            $object->hobby = $item['hobby'];
            $object->achievements = $item['other_info'];
            $object->highschool_name = $item['highscool_name'];
            $object->sports_rank = $item['sport_rank'];
            $object->work_place = $item['work_place'];
            $object->profession = $item['profession'];
            $object->category = $categories[$item['kategory']];
            $object->education_level = $education[$item['education']];
            $object->marital = $marital[$item['household']];
            $object->public_work = $item['public_work'];
            $object->tournaments = $item['tournament_ref'];
            $object->is_veteran = $item['veteran'] == 1 ? 1 : 0;

            $object->is_visible = $item['veteran'] == 3 ? 0 : 1;

            if(!empty($item['photo_path']) && $item['photo_path'] !== '../Images/photo_referees/no_photo.gif'){
                $path = str_replace('..', 'http://kfa.spb.ru', $item['photo_path']);

                $content = file_get_contents($path);

                $name = UtilHelper::generateFileName() . '.gif';
                $imagePath = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'referee'.DIRECTORY_SEPARATOR;

                $handler = fopen($imagePath . $name, 'w');
                fwrite($handler, $content);
                fclose($handler);

                $object->image = $name;
            }

            $object->save(false);

            if(!empty($item['kfk_assist'])){
                $careerFields = array(
                    'kfk_assist' => array(1, 0),
                    'kfk_main' => array(1, 1),
                    'dubl_assist' => array(2, 0),
                    'dubl_main' => array(2, 1),
                    '2div_assist' => array(3, 0),
                    '2div_main' => array(3, 1),
                    '1div_assist' => array(4, 0),
                    '1div_main' => array(4, 1),
                    'pl_assist' => array(5, 0),
                    'pl_main' => array(5, 1),
                    'fifa_assist' => array(6, 0),
                    'fifa_main' => array(6, 1),
                );
                foreach($careerFields as $field => $value){
                    if($year = $item[$field]){
                        Yii::app()->db->createCommand()->insert('referee_career', array(
                            'referee_id'=>$object->id,
                            'division_id'=>$value[0],
                            'status'=>$value[1],
                            'year'=>$year,
                        ));
                    }
                }
            }
        }

        Yii::app()->end();
    }

    public function actionCreateThumbs()
    {
        $data = Yii::app()->db->createCommand()
            ->select('image')
            ->from('referee')
            ->queryAll();

        $imagePath = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'referee'.DIRECTORY_SEPARATOR;

        $tinyThumbDirectory = Yii::getPathOfAlias('webroot.data.referee.thumb_t') . DIRECTORY_SEPARATOR;
        $smallThumbDirectory = Yii::getPathOfAlias('webroot.data.referee.thumb_s') . DIRECTORY_SEPARATOR;
        $mediumThumbDirectory = Yii::getPathOfAlias('webroot.data.referee.thumb_m') . DIRECTORY_SEPARATOR;

        foreach($data as $row) {
            if(empty($row['image'])) {
                continue;
            }

            Yii::app()->imageHandler
                ->load($imagePath . $row['image'])
                ->adaptiveThumb(50, 50)
                ->save($tinyThumbDirectory . $row['image'])
                ->reload()
                ->adaptiveThumb(80, 80)
                ->save($smallThumbDirectory . $row['image'])
                ->reload()
                ->adaptiveThumb(120, 120)
                ->save($mediumThumbDirectory . $row['image']);
        }
    }
}