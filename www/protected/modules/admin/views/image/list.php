<?
/**
 * @var ImageController $this
 * @var CActiveDataProvider $dataProvider
 */
?>
    <div class="row text-right">
        <?=YHtml::createButton($this->createUrl('create'))?>
    </div>
<?
    $this->widget('YGridView', array(
        'dataProvider' => $dataProvider,
        'buttons' => $this->getAction()->buttons,
        'columns' => array(
            array(
                'name' => '#',
                'value' => '$data->id',
            ),
            array(
                'name' => 'Дата изменения',
                'value' => '$data->getModifiedDateTime()',
            ),
            array(
                'name' => 'Превью',
                'type' => 'raw',
                'value' => '$data->getThumbHtml(ImageModelBehavior::SIZE_PREFIX_TINY, 80)',
            ),
            array(
                'name' => 'Название',
                'value' => '$data->name',
            ),
        ),
    ));
?>