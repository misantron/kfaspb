<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'КФА Спб',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.form.*',
		'application.models.behaviors.*',
		'application.components.*',
		'application.components.widgets.*',
		'application.helpers.*',
        'application.widgets.*',
        'application.actions.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

        'admin' => array(
            'class' => 'application.modules.admin.AdminModule',
        ),

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'111',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
            'class' => 'WebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array('admin/user/login'),
            'returnUrl' => array('/admin/referee/list'),
		),
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => array('<controller>/<action>', 'urlSuffix'=>'.html', 'caseSensitive'=>true),
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<action:\w+>' => 'site/<action>',
            ),
        ),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=kfa',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
//                array(
//                    'class'=>'CWebLogRoute',
//                    'categories'=>'system.db.CDbCommand',
//                    'showInFireBug'=>true,
//                ),
			),
		),
        'imageHandler' => array(
            'class' => 'ImageHandler',
        ),
        'clientScript' => array(
            'coreScriptPosition' => CClientScript::POS_END,
            'scriptMap' => array(
                'jquery.js' => '/js/jquery.js'
            ),
            'packages' => array(
                'bootstrap' => array(
                    'baseUrl' => '/',
                    'js' => array('js/bootstrap.js'),
                    'css' => array('css/bootstrap.css'),
                    'depends' => array('jquery'),
                ),
            ),
        ),
	),

	'params'=>array(

    ),
);