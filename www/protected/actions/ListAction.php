<?php

class ListAction extends BaseAction
{
    public $view = 'list';
    public $actionTitle = 'список';

    public $dataProviderClass = 'CActiveDataProvider';
    public $dataProviderConfig = array();
    public $buttons = array(
        YButtonColumn::BUTTON_EDIT,
        YButtonColumn::BUTTON_VISIBLE,
        YButtonColumn::BUTTON_HIDE,
        YButtonColumn::BUTTON_DELETE,
    );

    public function run()
    {
        /**
         * @var AdminController $controller
         * @var BaseModel $model
         * @var CActiveDataProvider $dataProviderClass
         */
        $controller = $this->getController();
        $modelClass = $controller->getModelClass();
        $dataProviderClass = $this->dataProviderClass;

        $controller->setPageTitle($controller->title . ' - ' . $this->actionTitle);

        $this->dataProviderConfig['pagination'] = array('pageSize' => $controller->getPageSize());

        $dataProvider = new $dataProviderClass($modelClass, $this->dataProviderConfig);

        $controller->render($this->view, array(
            'dataProvider' => $dataProvider,
        ));
    }
}