<?php

class ViewAction extends BaseAction
{
    public $view = 'view';
    public $actionTitle = 'просмотр';

    public $criteria = array();

    public function run()
    {
        /**
         * @var AdminController $controller
         * @var BaseModel $model
         */
        $controller = $this->getController();
        $modelClass = $controller->getModelClass();

        $model = $modelClass::model()->getById();
        if($model === null){
            $controller->setErrorMessage(Yii::t('', 'Объект не найден'));
            $controller->returnUrlRedirect();
        }

        $controller->setPageTitle($controller->title . ' - ' . $model->getName());

        $controller->render($this->view, array(
            'model' => $model,
        ));
    }
}