<?
/**
 * @var RefereeCommissionWidget $this
 */
?>
<div class="form-group">
    <div class="col-sm-3 control-label">
        <strong>Комитеты</strong>
    </div>
    <div class="col-sm-9 form-inline">
        <? foreach($this->items as $model): ?>
            <? /* @var RefereeCommission $model */ ?>
            <div style="margin-bottom: 5px">
                <div class="input-group">
                    <div class="input-group-addon"><?=$model->getAttributeLabel('commission_id')?></div>
                    <?=CHtml::activeDropDownList($model, 'commission_id', DictHelper::getList('commission'), array('class' => 'form-control'))?>
                </div>
                <div class="input-group">
                    <div class="input-group-addon"><?=$model->getAttributeLabel('status')?></div>
                    <?=CHtml::activeDropDownList($model, 'status', DictHelper::getList('commission_status'), array('class' => 'form-control'))?>
                </div>
                <?=CHtml::button('Удалить', array('class' => 'btn btn-danger'))?>
            </div>
        <? endforeach; ?>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <?=CHtml::button('Добавить', array('class' => 'btn btn-success'))?>
    </div>
</div>