<?php

class UserController extends AdminController
{
    protected $_modelClass = 'LoginForm';

    public function init()
    {
        $this->layout = '/layouts/empty';
        Yii::app()->clientScript->registerPackage('login');
    }

    public function actionLogin()
    {
        $this->setPageTitle('Авторизация');

        $model = new LoginForm;

        $this->ajaxValidation($model);

        if(Yii::app()->request->isPostRequest){
            $data = Yii::app()->request->getParam($this->getModelClass(), null);
            if($data !== null){
                $model->setAttributes($data);
                if($model->validate() && $model->login()){
                    $this->redirect($this->createUrl('/admin/referee/list'));
                }
            }
        }

        $this->render('login', array(
            'model' => $model,
        ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->user->loginUrl);
    }
}