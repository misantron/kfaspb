<?
/**
 * @var array $models
 */
?>
<div class="content-block">
    <h1>Арбитры</h1>
    <? foreach($models as $model): ?>
        <? $this->widget('application.widgets.RefereeWidget', array('itemId' => $model->id, 'thumbSize' => ImageModelBehavior::SIZE_PREFIX_SMALL)); ?>
    <? endforeach; ?>
</div>