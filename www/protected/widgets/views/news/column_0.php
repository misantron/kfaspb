<?
/**
 * @var string $title
 * @var string $brief
 */
?>
<div class="media">
    <div class="media-body">
        <?php if(!empty($title)): ?>
            <h2><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php echo nl2br($brief); ?>
    </div>
</div>