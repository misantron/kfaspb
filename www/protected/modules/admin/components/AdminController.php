<?php

class AdminController extends BaseController
{
    protected $_pageSize = 20;

    public $defaultAction = 'list';

    public function init()
    {
        $this->layout = '/layouts/main';
        Yii::app()->clientScript->registerPackage('main');
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'actions' => array('login'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
}