<?php

class WebUser extends CWebUser
{
    protected $_model;

    public function getModel()
    {
        if(!isset($this->id)) $this->_model = new User;
        if(!isset($this->_model)){
            $this->_model = User::model()->findByPk($this->id);
        }
        return $this->_model;
    }
} 