<?php

/**
 * This is the model class for table "stadium".
 *
 * The followings are the available columns in table 'stadium':
 * @property string $name
 * @property string $address
 * @property string $lat
 * @property string $lon
 * @property string $field_type
 * @property string $field_size
 */
class Stadium extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stadium';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_visible, field_type', 'numerical', 'integerOnly'=>true),
			array('name, address, field_size', 'length', 'max'=>255),
			array('lat, lon', 'length', 'max'=>18),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, address, field_type, field_size, lat, lon, is_visible', 'safe', 'on'=>'search'),
		);
	}

    public function behaviors()
    {
        return array(
            'defaultBehavior' => array(
                'class' => 'DefaultModelBehavior',
            ),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'clubs' => array(self::HAS_MANY, 'Club', 'stadium_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'address' => 'Адрес',
			'lat' => 'Широта',
			'lon' => 'Долгота',
			'field_type' => 'Тип покрытия',
			'field_size' => 'Размеры поля',
			'is_visible' => 'Активен',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('is_visible',$this->is_visible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stadium the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getArrayDataList()
    {
        $models = $this->visible()->findAll(array('order' => 'name'));
        return YHtml::listData($models, 'id', 'name');
    }

	/**
	 * @return string
	 */
	public function getViewUrl()
	{
		return Yii::app()->controller->createUrl('/stadium/view', array('id' => $this->id));
	}
}
