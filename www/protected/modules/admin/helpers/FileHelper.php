<?php

class FileHelper
{
    protected static $_fileTypes = array(
        'txt'  => 'Текстовый файл (.txt)',
        'pdf'  => 'Документ PDF (.pdf)',
        'zip'  => 'Сжатый архив ZIP (.zip)',
        'doc'  => 'Документ MS Word 2003 (.doc)',
        'docx' => 'Документ MS Word 2010 (.docx)',
        'xls'  => 'Таблица MS Excel 2003 (.xls)',
        'xlsx' => 'Таблица MS Excel 2010 (.xlsx)',
        'rar'  => 'Сжатый архив RAR (.rar)',
    );

    /**
     * @param int $bytes
     * @return string
     */
    public static function getSize($bytes)
    {
        if (!$bytes || $bytes < 0) {
            return '0 б';
        }

        if ($bytes < 1024) {
            return '1 Кб';
        }

        $sizeUnits = array('б', 'Кб', 'Мб', 'Гб');
        $unit = $sizeUnits[0];

        for ($i = 1; ($i < sizeof($sizeUnits) && $bytes >= 1024); $i++) {
            $bytes = $bytes / 1024;
            $unit = $sizeUnits[$i];
        }

        $round = $unit == 'Кб' ? 0 : 2;

        $formatted = round((float)$bytes, $round);

        return $formatted . ' ' . $unit;
    }

    /**
     * @param string $extension
     * @return string
     */
    public static function getType($extension)
    {
        return isset(static::$_fileTypes[$extension]) ? static::$_fileTypes[$extension] : '';
    }
}