<?php

class RefereeController extends AdminController
{
    protected $_modelClass = 'Referee';
    public $title = 'Арбитры';

    public function actions()
    {
        return array(
            'list' => array(
                'class' => 'ListAction',
                'dataProviderClass' => 'ApActiveDataProvider',
                'dataProviderConfig' => array(
                    'alphapagination'=>array(
                        'attribute' => 'last_name',
                        'activeCharSet' => Referee::model()->getActiveCharset(),
                    ),
                    'criteria' => array(
                        'order' => 'last_name',
                    ),
                ),
            ),
            'create' => array(
                'class' => 'CreateAction',
            ),
            'edit' => array(
                'class' => 'EditAction',
            ),
            'delete' => array(
                'class' => 'DeleteAction',
            ),
            'visibility' => array(
                'class' => 'VisibilityAction',
            ),
        );
    }

    public function actionSearch()
    {
        if(Yii::app()->request->isAjaxRequest){
            $response = new AjaxResponse();

            $pattern = Yii::app()->request->getParam('pattern', null);
            if(!$pattern){

            }

            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('last_name', $pattern, true, 'OR');
            $criteria->addSearchCondition('first_name', $pattern, true, 'OR');

            $data = array();
            $models = Referee::model()->findAll($criteria);
            foreach ($models as $model) {
                /* @var Referee $model */
                $data[] = array(
                    'id' => $model->id,
                    'url' => $model->getProfileUrl(),
                    'name' => trim($model->getFullName()),
                );
            }

            $response
                ->setResult(true)
                ->setMessage('Success')
                ->setData($data);

            $response->render();
        }
        throw new CHttpException(401, 'Bad request');
    }
}