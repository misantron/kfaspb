<?
/**
 * @var BrigadeController $this
 * @var CActiveDataProvider $dataProvider
 */

$this->widget('YGridView', array(
    'dataProvider' => $dataProvider,
    'buttons' => false,
    'columns' => array(
        array(
            'name' => 'Фамилия / Имя / Отчество',
            'value' => '$data->fullName'
        ),
        array(
            'name' => 'Дата рождения',
            'value' => '$data->birth_date'
        ),
        array(
            'name' => 'Категория',
            'value' => 'DictHelper::getById(\'category\', $data[category])',
        ),
        array(
            'name' => 'Должность',
            'value' => 'DictHelper::getById(\'brigade_status\', $data[brigade_status])',
        ),
    ),
));