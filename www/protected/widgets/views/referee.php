<?
/**
 * @var RefereeWidget $this
 */
?>
<div class="referee-widget media">
    <div class="media-left">
        <a href="<?=$this->model->getProfileUrl()?>" title="<?=$this->model->getFullName()?>">
            <img class="media-object" src="<?=$this->model->getThumbUrl($this->thumbSize)?>" alt="<?=$this->model->getFullName()?>" />
        </a>
    </div>
    <div class="media-body">
        <h3 class="media-heading"><?=$this->model->getFullName()?></h3>
        <div class="referee-profile-content">
            <? if($this->model->birth_date !== '0000-00-00'): ?>
            Дата рождения: <?=ViewHelper::getDate(strtotime($this->model->birth_date))?><br />
            <? endif; ?>
            <?=DictHelper::getById('category', $this->model->category)?>
        </div>
    </div>
</div>
<div class="clear"></div>