<?php

class BaseController extends CController
{
    const
        MESSAGE_SUCCESS = 'success',
        MESSAGE_ERROR   = 'danger'
    ;

    public $layout = '//layouts/column2';
    public $menu = array();
    public $breadcrumbs = array();

    public $title = '';

    protected $_modelClass = 'Base';
    protected $_childModelClasses = array();
    protected $_pageSize = 20;

    /**
     * @param BaseModel|CFormModel $model
     */
    public function ajaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax'] === $this->getFormId())
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function returnUrlRedirect()
    {
        $this->redirect($this->createUrl($this->defaultAction));
    }

    /**
     * @return string
     */
    public function getModelClass()
    {
        return $this->_modelClass;
    }

    /**
     * @return array
     */
    public function getChildModelClasses()
    {
        return $this->_childModelClasses;
    }

    /**
     * @return mixed
     */
    public function getReturnUrl()
    {
        return $this->createUrl($this->defaultAction);
    }

    /**
     * @return string
     */
    public function getFormId()
    {
        return strtolower($this->id) . '-form';
    }

    /**
     * @param string $text
     * @return bool
     */
    public function setSuccessMessage($text = '')
    {
        return Yii::app()->user->setFlash(self::MESSAGE_SUCCESS, $text);
    }

    /**
     * @param string $text
     * @return bool
     */
    public function setErrorMessage($text = '')
    {
        return Yii::app()->user->setFlash(self::MESSAGE_ERROR, $text);
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return Yii::app()->user->getFlashes();
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->_pageSize;
    }
}