<?php

/**
 * This is the model class for table "referee".
 *
 * @property integer $brigade_id
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property string $birth_date
 * @property string $birth_place
 * @property integer $category
 * @property string $referee_start_year
 * @property string $college_enter_year
 * @property integer $education_level
 * @property string $height
 * @property string $weight
 * @property string $email
 * @property string $language
 * @property integer $marital
 * @property integer $brigade_status
 * @property string $category_get_year
 * @property string $education_place
 * @property string $hobby
 * @property string $work_place
 * @property string $sports_rank
 * @property string $profession
 * @property string $highschool_name
 * @property integer $is_veteran
 * @property string $achievements
 * @property string $image
 * @property string $awards
 * @property string $web
 * @property string $public_work
 * @property string $tournaments
 * @property string $path
 */
class Referee extends BaseModel
{
    protected $fullName;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'referee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('last_name, first_name, patronymic', 'required'),
			array('brigade_id, category, education_level, marital, brigade_status, is_veteran', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, patronymic', 'length', 'max'=>64),
			array('birth_date', 'length', 'max'=>10),
			array('birth_place, education_place, hobby, highschool_name, public_work, tournaments, path', 'length', 'max'=>255),
			array('achievements', 'length', 'max'=>512),
			array('referee_start_year, college_enter_year, category_get_year', 'length', 'max'=>4),
			array('height, weight', 'length', 'max'=>3),
			array('email, language, work_place, sports_rank, profession', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, brigade_id, first_name, last_name, patronymic, birth_date, birth_place, category, referee_start_year, college_enter_year, education_level, height, weight, email, language, marital, brigade_status, category_get_year, education_place, hobby, work_place, sports_rank, profession, highschool_name, is_veteran, achievements, public_work, tournaments, path', 'safe', 'on'=>'search'),
		);
	}

    public function behaviors()
    {
        return array(
            'defaultBehavior' => array(
                'class' => 'DefaultModelBehavior',
            ),
            'imageBehavior' => array(
                'class' => 'ImageModelBehavior',
                'baseDirectory' => 'data.referee',
            ),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'commissions' => array(self::HAS_MANY, 'RefereeCommission', 'referee_id'),
            'career' => array(self::HAS_MANY, 'RefereeCareer', 'referee_id'),
            'brigade' => array(self::BELONGS_TO, 'Brigade', 'brigade_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => '#',
            'brigade_id' => 'Бригада',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'birth_date' => 'Дата рождения',
            'birth_place' => 'Место рождения',
            'category' => 'Категория',
            'referee_start_year' => 'Год начала судейства',
            'college_enter_year' => 'Год вступления в коллегию',
            'education_level' => 'Образование',
            'height' => 'Рост',
            'weight' => 'Вес',
            'email' => 'Email',
            'language' => 'Иностранные языки',
            'marital' => 'Семейное положение',
            'brigade_status' => 'Должность в бригаде',
            'category_get_year' => 'Год получения категории',
            'education_place' => 'Место обучения',
            'hobby' => 'Увлечения и хобби',
            'work_place' => 'Место работы',
            'sports_rank' => 'Спортивное звание',
			'profession' => 'Профессия',
			'highschool_name' => 'Учебные заведения',
			'is_veteran' => 'Ветеран',
			'achievements' => 'Достижения',
			'image' => 'Изображение',
			'awards' => 'Награды',
			'web' => 'Личный сайт',
			'public_work' => 'Общественная работа в коллегии',
			'tournaments' => 'Участие в турнирах',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('brigade_id',$this->brigade_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('patronymic',$this->patronymic,true);
		$criteria->compare('birth_date',$this->birth_date,true);
		$criteria->compare('birth_place',$this->birth_place,true);
		$criteria->compare('category',$this->category);
		$criteria->compare('referee_start_year',$this->referee_start_year,true);
		$criteria->compare('college_enter_year',$this->college_enter_year,true);
		$criteria->compare('education_level',$this->education_level);
		$criteria->compare('height',$this->height,true);
		$criteria->compare('weight',$this->weight,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('marital',$this->marital);
		$criteria->compare('brigade_status',$this->brigade_status);
		$criteria->compare('category_get_year',$this->category_get_year,true);
		$criteria->compare('education_place',$this->education_place,true);
		$criteria->compare('hobby',$this->hobby,true);
		$criteria->compare('work_place',$this->work_place,true);
		$criteria->compare('sports_rank',$this->sports_rank,true);
		$criteria->compare('profession',$this->profession,true);
		$criteria->compare('highschool_name',$this->highschool_name,true);
		$criteria->compare('is_veteran',$this->is_veteran);
		$criteria->compare('achievements',$this->achievements,true);
		$criteria->compare('awards',$this->awards,true);
		$criteria->compare('web',$this->web,true);
		$criteria->compare('public_work',$this->public_work,true);
		$criteria->compare('tournaments',$this->tournaments,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Referee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getActiveCharset()
    {
        $chars = Yii::app()->db->createCommand()
            ->select('DISTINCT(SUBSTR(`last_name`,1,1)) AS `name`')
            ->from($this->tableName())
            ->where('is_visible = 1')
            ->order('name')
            ->queryAll();

        $activeChars = array();
        foreach($chars as $char){
            $activeChars[] = mb_strtoupper($char['name']);
        }
        return $activeChars;
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->fullName = implode(' ', array($this->last_name, $this->first_name, $this->patronymic));
    }

    /**
     * @return string
     */
    public function getProfileUrl()
    {
        return Yii::app()->controller->createUrl('/referee/profile', array('id' => $this->id));
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return implode(' ', array($this->last_name, $this->first_name, $this->patronymic));
    }

    /**
     * @return int
     */
    public function getAge()
    {
        if(empty($this->birth_date)){
            return 0;
        }
        
        $year = date('Y', strtotime($this->birth_date));

        $age = date('md', strtotime($this->birth_date)) > date('md')
            ? ((date('Y') - $year) - 1)
            : (date('Y') - $year);

        return $age;
    }
}
