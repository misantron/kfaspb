<?
/**
 * @var Referee $model
 */
?>
<div class="content-block no-padding">
    <div class="referee-profile-header">
        <div class="referee-profile-image">
            <img src="<?=$model->getImageUrl()?>" alt="<?=$model->getFullName() ?>">
        </div>
        <h2><?=$model->getFullName() ?></h2>
        <div class="referee-profile-contacts">
            <? if($model->email): ?>
            <a href="mailto:<?=$model->email?>" class="fa fa-envelope" target="_blank"></a>
            <? endif; ?>
            <? if($model->web): ?>
            <a href="http://<?=$model->web?>" class="fa fa-globe" target="_blank"></a>
            <? endif; ?>
        </div>
    </div>
    <div class="referee-profile-container">
        <? if($model->birth_date !== '0000-00-00'): ?>
        <div class="row">
            <label class="col-sm-6">Дата рождения</label>
            <div class="col-sm-6">
                <?=ViewHelper::getDate(strtotime($model->birth_date))?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->birth_place)): ?>
        <div class="row">
            <label class="col-sm-6">Место рождения</label>
            <div class="col-sm-6">
                <?=$model->birth_place?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->marital)): ?>
        <div class="row">
            <label class="col-sm-6">Семейное положение</label>
            <div class="col-sm-6">
                <?=DictHelper::getById('marital', $model->marital)?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->education_level)): ?>
        <div class="row">
            <label class="col-sm-6">Образование</label>
            <div class="col-sm-6">
                <?=DictHelper::getById('education', $model->education_level)?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->language)): ?>
        <div class="row">
            <label class="col-sm-6">Иностранные языки</label>
            <div class="col-sm-6">
                <?=$model->language?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->height) && !empty($model->weight)): ?>
        <div class="row">
            <label class="col-sm-6">Рост / вес</label>
            <div class="col-sm-6">
                <?=$model->height?> см / <?=$model->weight?> кг
            </div>
        </div>
        <? endif; ?>
        <div class="divider"></div>
        <? if(!empty($model->category)): ?>
        <div class="row">
            <label class="col-sm-6">Категория (год получения)</label>
            <div class="col-sm-6">
                <?=DictHelper::getById('category', $model->category)?>
                <? if(!empty($model->category_get_year)): ?>
                (<?=$model->category_get_year?> год)
                <? endif; ?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->college_enter_year)): ?>
        <div class="row">
            <label class="col-sm-6">Год вступления в коллегию (начала судейства)</label>
            <div class="col-sm-6">
                <?=$model->college_enter_year?>
                <? if($model->college_enter_year != $model->referee_start_year): ?>
                    <?='(' . $model->referee_start_year . ')';?>
                <? endif; ?>
                год
            </div>
        </div>
        <? endif; ?>
        <? if($model->brigade instanceof Brigade): ?>
            <div class="row">
                <label class="col-sm-6">Бригада</label>
                <div class="col-sm-6">
                    <a href="<?=$model->brigade->getViewUrl()?>">#<?=$model->brigade_id?></a>
                    <? if(!empty($model->brigade_status)): ?>
                        <?='('.mb_strtolower(DictHelper::getById('brigade_status', $model->brigade_status), 'utf-8').')';?>
                    <? endif; ?>
                </div>
            </div>
        <? endif; ?>
        <? if(!empty($model->public_work)): ?>
        <div class="row">
            <label class="col-sm-6">Общественная работа в коллегии</label>
            <div class="col-sm-6">
                <?=$model->public_work?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->commissions)): ?>
        <div class="row">
            <label class="col-sm-6">Участие в коммисиях</label>
            <div class="col-sm-6">
                <? foreach($model->commissions as $row): ?>
                    <?=DictHelper::getById('commission', $row->commission_id);?>
                    (<?=DictHelper::getById('commission_status', $row->status);?>)
                    <br />
                <? endforeach; ?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->career) || !empty($model->tournaments)): ?>
        <div class="divider"></div>
        <? endif; ?>
        <? if(!empty($model->career)): ?>
        <div class="row">
            <div class="col-sm-12">
                <h2>Судейская деятельность</h2>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Помощник судьи</th>
                                <th>Главный судья</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach($model->career as $key => $item): ?>
                            <tr>
                                <td>
                                    <?=DictHelper::getById('division', $key)?>
                                </td>
                                <td>
                                    <? if(isset($item[0])): ?>
                                    c <?=$item[0];?> года
                                    <? endif; ?>
                                </td>
                                <td>
                                    <? if(isset($item[1])): ?>
                                    c <?=$item[1];?> года
                                    <? endif; ?>
                                </td>
                            </tr>
                            <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->tournaments)): ?>
        <div class="row">
            <label class="col-sm-6">Участие в турнирах</label>
            <div class="col-sm-6">
                <?=$model->tournaments;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->education_place) || !empty($model->highschool_name) || !empty($model->profession) || !empty($model->work_place)): ?>
        <div class="divider"></div>
        <div class="row">
            <div class="col-sm-12">
                <h2>Работа и образование</h2>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->education_place)): ?>
        <div class="row">
            <label class="col-sm-6">Место учёбы</label>
            <div class="col-sm-6">
                <?=$model->education_place;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->highschool_name)): ?>
        <div class="row">
            <label class="col-sm-6">Окончил(а)</label>
            <div class="col-sm-6">
                <?=$model->highschool_name;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->profession)): ?>
        <div class="row">
            <label class="col-sm-6">Специальность</label>
            <div class="col-sm-6">
                <?=$model->profession;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->work_place)): ?>
        <div class="row">
            <label class="col-sm-6">Место работы</label>
            <div class="col-sm-6">
                <?=$model->work_place;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->sports_rank) || !empty($model->awards) || !empty($model->achievements) || !empty($model->hobby)): ?>
        <div class="divider"></div>
        <div class="row">
            <div class="col-sm-12">
                <h2>Дополнительная информация</h2>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->sports_rank)): ?>
        <div class="row">
            <label class="col-sm-6">Звание (спортивное или учёное)</label>
            <div class="col-sm-6">
                <?=$model->sports_rank;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->awards)): ?>
        <div class="row">
            <label class="col-sm-6">Награды</label>
            <div class="col-sm-6">
                <?=$model->awards;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->achievements)): ?>
        <div class="row">
            <label class="col-sm-6">Достижения</label>
            <div class="col-sm-6">
                <?=$model->achievements;?>
            </div>
        </div>
        <? endif; ?>
        <? if(!empty($model->hobby)): ?>
        <div class="row">
            <label class="col-sm-6">Увлечения и хобби</label>
            <div class="col-sm-6">
                <?=$model->hobby;?>
            </div>
        </div>
        <? endif; ?>
    </div>
</div>