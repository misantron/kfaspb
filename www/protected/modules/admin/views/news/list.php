<?
/**
 * @var NewsController $this
 * @var CActiveDataProvider $dataProvider
 */
?>
    <div class="row text-right">
        <?=YHtml::createButton($this->createUrl('create'))?>
    </div>
<?
    $this->widget('YGridView', array(
        'dataProvider' => $dataProvider,
        'buttons' => $this->getAction()->buttons,
        'columns' => array(
            array(
                'name' => 'published',
                'value' => 'date("d.m.Y", $data->published)',
                'type' => 'raw',
            ),
            array(
                'name' => 'title',
                'value' => '$data->title',
            ),
            array(
                'name' => 'brief',
                'value' => '$data->brief',
            ),
        ),
    ));
?>