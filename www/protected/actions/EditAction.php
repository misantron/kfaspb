<?php

class EditAction extends BaseAction
{
    public $view = 'form';
    public $actionTitle = 'редактировать';

    public $criteria = array();

    public function run()
    {
        /**
         * @var AdminController $controller
         * @var BaseModel $model
         */
        $controller = $this->getController();
        $modelClass = $controller->getModelClass();
        $childModelClasses = $controller->getChildModelClasses();

        $controller->setPageTitle($controller->title . ' - ' . $this->actionTitle);

        $model = $modelClass::model()->getById();
        if($model === null){
            $controller->setErrorMessage(Yii::t('', 'Объект не найден'));
            $controller->returnUrlRedirect();
        }
        $model->setScenario(BaseModel::ACTION_UPDATE);

        $controller->ajaxValidation($model);

        if($data = Yii::app()->request->getParam($modelClass, null)){
            foreach($childModelClasses as $className){
                $data['_relations'][$className] = Yii::app()->request->getParam($className, null);
            }
            $model->setAttributes($data);
            if($model->validate()){
                if($model->save(false)){
                    $controller->setSuccessMessage(Yii::t('', 'Объект успешно изменен'));
                } else {
                    $controller->setErrorMessage(Yii::t('', 'Ошибка при редактировании объекта'));
                }
                $controller->returnUrlRedirect();
            }
        }

        $controller->render($this->view, array(
            'model' => $model,
        ));
    }
}