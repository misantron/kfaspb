<?php

/**
 * This is the model class for table "file".
 *
 * @property string $name
 * @property integer $group_id
 * @property string $description
 * @property string $filename
 * @property string $path
 * @property string $extension
 * @property string $mime_type
 * @property integer $byte_size
 * @property string $hash
 */
class File extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'file';
	}

    public function behaviors()
    {
        return array(
            'defaultBehavior' => array(
                'class' => 'DefaultModelBehavior',
            ),
            'fileBehavior' => array(
                'class' => 'FileModelBehavior',
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('is_visible, group_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 128),
            array('description', 'length', 'max' => 1000),
            array('mime_type', 'length', 'max' => 128),
            array('byte_size', 'length', 'max' => 20),
//            array('file', 'file',
//                'types' => 'doc,docx,xls,xlsx,txt,zip,pdf,rar',
//                'wrongType' => 'Файл "{file}" имеет недопустимый тип. Для загрузки возможны только файлы с расширениями: {extensions}.',
//                'maxSize' => 2097152,
//                'tooLarge' => 'Файл "{file}" слишком большой. Максимальный размер загружаемого файла - {limit} байт.',
//                'allowEmpty' => true,
//                'message' => 'Необходимо выбрать файл для загрузки.',
//                'maxFiles' => 1,
//                'tooMany' => 'Можно загрузить только {limit} файл.',
//            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, group_id, name, description, mime_type, byte_size, is_visible', 'safe', 'on' => 'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Раздел',
			'name' => 'Название',
			'description' => 'Описание',
			'mime_type' => 'Тип',
			'byte_size' => 'Размер',
			'is_visible' => 'Видимость',
			'file' => 'Файл',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('group_id',$this->group_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('mime_type',$this->mime_type,true);
		$criteria->compare('byte_size',$this->byte_size,true);
		$criteria->compare('is_visible',$this->is_visible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
