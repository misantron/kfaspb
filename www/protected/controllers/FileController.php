<?php

class FileController extends BaseController
{
    public function actionList()
    {
        $data = array();
        $groupList = DictHelper::getList('document_group');

        foreach ($groupList as $groupId => $title) {
            $list = File::model()->visible()->findAllByAttributes(array(
                'group_id' => $groupId
            ));
            if(!empty($list)){
                $data[] = array(
                    'title' => $title,
                    'data' => $list,
                );
            }
        }

        $this->render('list', array(
            'data' => $data
        ));
    }
}